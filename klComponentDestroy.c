// 
// FILE            klComponentDestroy.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <strings.h>                         // bzero
#include <stdlib.h>                          // free

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klHooks.h"                    // KL_ERROR_HOOK
#include "klog/klComponentDestroy.h"         // Own interface



// -----------------------------------------------------------------------------
//
// klComponentDestroy -
//
// The validity of compP is checked - must be one of the items in the vector
//
KlStatus klComponentDestroy(KlComponent* compP)
{
  int ix;

  for (ix = 0; ix < K_VEC_SIZE(klCompV); ix++)
  {
    if (compP == &klCompV[ix])
    {
      if (compP->name != NULL)
        free(compP->name);

      //
      // FIXME: free up somehow the trace levels used by this component:
      //        compP->offset - compP->offset + compP->tLevs
      //
      bzero(compP, sizeof(KlComponent));
      compP->inUse = KFALSE;

      return KlsOk;
    }
  }

  KL_ERROR_HOOK(KlsComponentNotFound, "component not found");
  return KlsComponentNotFound;
}
