// 
// FILE            klFreeText.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdarg.h>                          // ellipses va_*
#include <stdio.h>                           // vsnprintf

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klFreeText.h"                 // Own interface



// -----------------------------------------------------------------------------
//
// freeTextBuffer -
//
static char freeTextBuffer[KL_FREETEXT_MAX_SIZE];



// -----------------------------------------------------------------------------
//
// klFreeText - all the free-text into freeTextBuffer
//
// NOTE
//   It is OK to use a single buffer for this as klFreeText is only called with
//   the klog semaphore taken.
//
char* klFreeText(const char* format, ...)
{
  va_list  args;
  
  va_start(args, format);
  vsnprintf(freeTextBuffer, KL_FREETEXT_MAX_SIZE, format, args);
  va_end(args);

  // FIXME: Check for buffer too small?
  return freeTextBuffer;
}
