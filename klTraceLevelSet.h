// 
// FILE            klTraceLevelSet.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLTRACELEVELSET_H_
#define KLOG_KLTRACELEVELSET_H_

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/KlComponent.h"                // KlComponent



// -----------------------------------------------------------------------------
//
// klTraceLevelSet -
//
// Format of 'traceLevels' is: "t1-t2,t3,t4-t5,t6".
//
// I.e. a comma separated list of numbers and ranges.
// Two special values: "ALL" and "NONE"
// The input string can *only* contain the chars hyphen, comma, and digits: -,0-9
//
extern KlStatus klTraceLevelSet(KlComponent* compP, char* traceLevels);

#endif  // KLOG_KLTRACELEVELSET_H_
