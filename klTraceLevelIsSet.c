// 
// FILE            klTraceLevelIsSet.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kTypes.h"                    // KBool

#include "klog/klInit.h"                     // KL_INITALIZED_CHECK
#include "klog/klHooks.h"                    // Hooks ...
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klTraceLevelV.h"              // klTraceLevelV, KL_TRACE_LEVELS
#include "klog/klTraceLevelIsSet.h"          // Own interface



// -----------------------------------------------------------------------------
//
// klTraceLevelIsSet - is a trace level set in a component?
//
KBool klTraceLevelIsSet(KlComponent* compP, unsigned int traceLevel)
{
  KL_INITALIZED_CHECK(KFALSE);

  if (compP == NULL)
  {
    KL_ERROR_HOOK(KlsInvalidParameter, "Component is NULL");
    return KFALSE;
  }

  if (traceLevel >= K_VEC_SIZE(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsInvalidTraceLevel, "single value for trace level is off the charts");
    return KFALSE;
  }

  int vectorIx = compP->offset + traceLevel;
  if (vectorIx >= KL_TRACE_LEVELS)
  {
    KL_ERROR_HOOK(KlsInvalidOffset, "absolute trace level index for single value for trace level is off the charts");
    return KFALSE;
  }

  if (traceLevel >= compP->tLevs)
  {
    KL_ERROR_HOOK(KlsInvalidParameter, "single value for trace level exceeds the number of trace levels registered for component");
    return KFALSE;
  }

  // Now, we attack the trace level vector, as all is OK
  return klTraceLevelV[vectorIx];
}
