// 
// FILE            klSem.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <semaphore.h>                       // sem_init, sem_wait, sem_post

#include "kbase/kBasicLog.h"                 // Basic log macros using printf

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klSem.h"                      // Own interface



// -----------------------------------------------------------------------------
//
// sem - 
//
static sem_t sem;



// -----------------------------------------------------------------------------
//
// semInitialized - has sem_init successfully been called already?
//
static KBool semInitialized = KFALSE;



// -----------------------------------------------------------------------------
//
// semInit - 
//
KlStatus klSemInit(void)
{
  if (semInitialized == KFALSE)
  {
    if (sem_init(&sem, 0, 1) != 0)
      return KlsSemInit;

    semInitialized = KTRUE;
  }

  return KlsOk;
}



// -----------------------------------------------------------------------------
//
// klSemDestroy - cleanup
//
void klSemDestroy(void)
{
  sem_destroy(&sem);

  semInitialized = KFALSE;
}



// -----------------------------------------------------------------------------
//
// klSemTake - 
//
KlStatus klSemTake(void)
{
  if (sem_wait(&sem) != 0)
  {
    KBL_E(("Error taking sem"));
    return KlsSemTake;
  }

  return KlsOk;
}



// -----------------------------------------------------------------------------
//
// klSemGive - 
//
KlStatus klSemGive(void)
{
  if (sem_post(&sem) != 0)
  {
    KBL_E(("Error giving sem"));
    return KlsSemGive;
  }

  return KlsOk;
}
