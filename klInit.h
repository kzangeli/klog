// 
// FILE            klInit.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLINIT_H_
#define KLOG_KLINIT_H_

#include "kbase/kTypes.h"                    // KBool

#include "klog/KlStatus.h"                   // KlStatus



// -----------------------------------------------------------------------------
//
// KL_INITALIZED_CHECK - make sure that klog is initialized
//
// Note that no error hook can be used if the library isn't initialized ...
//
#define KL_INITALIZED_CHECK(r)                             \
do                                                         \
{                                                          \
  if (klogInitialized != KTRUE)                            \
    return r;                                              \
} while (0)



// -----------------------------------------------------------------------------
//
// klogInitialized - is the library already initialized?
//
extern KBool klogInitialized;



// -----------------------------------------------------------------------------
//
// Output file descriptors
//
extern int klLogFd;
extern int klScreenFd;



// -----------------------------------------------------------------------------
//
// klInit - 
//
extern KlStatus klInit(char* _progName, char* logDir, KBool toFile, char* _logFileName);



// -----------------------------------------------------------------------------
//
// klExit - cleanup
//
extern void klExit(void);

#endif  // KLOG_KLINIT_H_
