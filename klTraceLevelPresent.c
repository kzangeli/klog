// 
// FILE            klTraceLevelPresent.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                           // printf

#include "kbase/kMacros.h"                   // K_FT
#include "kbase/kStringSplit.h"              // kStringSplit

#include "klog/klComponentLookup.h"          // klComponentLookup
#include "klog/klTraceLevelPresent.h"        // Own interface



// -----------------------------------------------------------------------------
//
// traceLevelPresent -
//
static void traceLevelPresent(KlComponent* compP)
{
  int ix;

  for (ix = 0; ix < compP->tLevs; ix++)
  {
    printf("  %02d: %s (global index: %d)\n", compP->tLevInfoV[ix].ix, compP->tLevInfoV[ix].name, compP->offset + compP->tLevInfoV[ix].ix);
  }
  printf("\n");
}



// -----------------------------------------------------------------------------
//
// klTraceLevelPresent - present info on all trace levels for components in 'compList'
//
// NOTE:
//   This function uses 'printf' to 'stdout' to present its result
//
void klTraceLevelPresent(char* compList)
{
  char* compNameV[20];
  int   components;

  if ((compList == NULL) || (*compList == 0) || (strcmp(compList, "All") == 0))
  {
    // Present trace levels for ALL components
    int          ix;
    KlComponent* compP;

    for (ix = 0; ix < klCompNo; ix++)
    {
      compP = &klCompV[ix];
      printf("%s: (%d levels)\n", compP->name, compP->tLevs);
      traceLevelPresent(compP);
    }
    return;
  }


  //
  // Present trace levels for the components present in 'compNameV'
  //
  components = kStringSplit(compList, ',', compNameV, 20);

  int ix;
  for (ix = 0; ix < components; ix++)
  {
    KlComponent* compP;

    compP = klComponentLookup(compNameV[ix]);
    if (compP == NULL)
      printf("%s: not a component\n", compNameV[ix]);
    else
    {
      printf("%s: (%d levels)\n", compP->name, compP->tLevs);
      traceLevelPresent(compP);
    }
  }
}
