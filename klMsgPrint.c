// 
// FILE            klMsgPrint.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <unistd.h>                          // write

#include "kbase/kBasicLog.h"                 // Basic log macros using printf

#include "klog/KlStatus.h"                   // KlsWriteToLogFile, KlsWriteToScreen, etc.
#include "klog/klInit.h"                     // klLogFd
#include "klog/klConfig.h"                   // klScreenFd
#include "klog/klConfigVars.h"               // klLogFileFormat, klScreenFormat, klErrorsToStderr, klWarningsToStderr
#include "klog/klHooks.h"                    // error and log hooks
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klFreeText.h"                 // KL_FREETEXT_MAX_SIZE
#include "klog/klMsgPrint.h"                 // Own interface



// -----------------------------------------------------------------------------
//
// KL_MSG_MAX_SIZE -
//
#define KL_MSG_MAX_SIZE (KL_FREETEXT_MAX_SIZE + 256)



// -----------------------------------------------------------------------------
//
// msgBuf
//
static char msgBuf[KL_MSG_MAX_SIZE];



// -----------------------------------------------------------------------------
//
// klMsgPrint -
//
// 'void' as ... if anything goes wrong, a callback to KlComponent::errorF is done
//
// Last Parameter 'number' contains an integer with different meanings, depending or 'type'
//   - 'T':  trace level
//   - 'P':  errno
//   - 'F':  severity of the FIXME
//
void klMsgPrint
(
  KlComponent*  compP,
  char          type,
  const char*   fileName,
  int           lineNo,
  const char*   functionName,
  char*         message,
  int           number
)
{
  int traceLevel = number;  // For type == 'T'
  int rNo        = number;  // For type == 'P'
  int severity   = number;  // For type == 'F'

  // FIXME: Add Timestamp
  // FIXME: Use the logFormat given lo klConfig
  // FIXME: Use the timeFormat given lo klConfig


  //
  // <TEMP>
  //   func tests need no __LINE__ ... only makes the diff more difficult
  //   Until logFormat is implemented, just zero out the log-line if inside test (logFormat == "TYPE: COMP:FILE[0]:FUNC: MSG")
  //
  if ((klLogFileFormat != NULL) && (strcmp(klLogFileFormat, "TYPE: COMP:FILE[0]:FUNC: MSG") == 0))
    lineNo = 0;

  //
  // </TEMP>
  //


  if (type == 'T')
    snprintf(msgBuf, sizeof(msgBuf), "T%d: %s:%s[%d]:%s: %s\n", traceLevel, compP->name, fileName, lineNo, functionName, message);
  else if (type == 'P')
    snprintf(msgBuf, sizeof(msgBuf), "E: %s:%s[%d]:%s: %s: %s\n", compP->name, fileName, lineNo, functionName, message, strerror(rNo));
  else if (type == 'F')
    snprintf(msgBuf, sizeof(msgBuf), "F%d: %s:%s[%d]:%s: %s\n", severity, compP->name, fileName, lineNo, functionName, message);
  else
    snprintf(msgBuf, sizeof(msgBuf), "%c: %s:%s[%d]:%s: %s\n", type, compP->name, fileName, lineNo, functionName, message);

  int msgBufLen = strlen(msgBuf);

  if (klLogFd != -1)
  {
    int nb = write(klLogFd, msgBuf, msgBufLen);

    if (nb == -1)
    {
      KL_ERROR_HOOK(KlsWriteToLogFile, "error writing to log file");
      KBL_P(("write to log file"));
    }
    else if (nb != msgBufLen)
    {
      KL_ERROR_HOOK(KlsPartialWriteToLogFile, "partial write to log file");
      KBL_P(("written %d bytes to log file, not %d", nb, msgBufLen));
    }
  }

  if (klScreenFd != -1)
  {
    int screenFd = klScreenFd;
    
    if ((klErrorsToStderr == KTRUE) && ((type == 'E') || (type == 'P')))
      screenFd = 2;  // stderr;
    if ((klWarningsToStderr == KTRUE) && (type == 'W'))
      screenFd = 2;  // stderr;
      
    int nb = write(screenFd, msgBuf, msgBufLen);

    if (nb == -1)
    {
      KL_ERROR_HOOK(KlsWriteToScreen, "error writing to screen");
      KBL_P(("write to screen"));
    }
    else if (nb != msgBufLen)
    {
      KL_ERROR_HOOK(KlsPartialWriteToScreen, "partial write to screen");
      KBL_P(("written %d bytes to screen, not %d", nb, msgBufLen));
    }
  }
}
