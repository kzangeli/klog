// 
// FILE            klDestroy.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdlib.h>                          // free
#include <unistd.h>                          // close

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klInit.h"                     // klLogFd, klExit
#include "klog/klSem.h"                      // klSemDestroy
#include "klog/klConfig.h"                   // klScreenFd
#include "klog/klConfigVars.h"               // klLogFileFormat, klScreenFormat
#include "klog/klHooks.h"                    // hooks
#include "klog/KlComponent.h"                // KlComponent, klCompV
#include "klog/klTraceLevelV.h"              // klTraceLevelOffset
#include "klog/klDestroy.h"                  // Own interface



// -----------------------------------------------------------------------------
//
// klDestroy - free all allocated memory and reset all buffers
//
// After destroying, klog should only be used for testing purposes - unit tests
//
KlStatus klDestroy(void)
{
  if (klLogFileFormat != NULL)
  {
    free(klLogFileFormat);
    klLogFileFormat = NULL;
  }

  if (klScreenFormat != NULL)
  {
    free(klScreenFormat);
    klScreenFormat = NULL;
  }

  int compIx;
  for (compIx = 0; compIx < K_VEC_SIZE(klCompV); compIx++)
  {
    KlComponent* compP = &klCompV[compIx];

    if (compP->inUse == KFALSE)
      continue;

    if (compP->name != NULL)
      free(compP->name);
  }

  bzero(klCompV,       sizeof(klCompV));
  bzero(klTraceLevelV, sizeof(klTraceLevelV));

  klTraceLevelOffset = 0;
  klCompNo           = 0;
  klLogHookF         = NULL;
  klLogHookP         = NULL;
  klErrorHookF       = NULL;
  klErrorHookP       = NULL;
  klTraceLevelOffset = 0;

  if (klLogFd != 0)
    close(klLogFd);

  klLogFd    = -1;
  klScreenFd = -1;

  klExit();
  klSemDestroy();

  return KlsOk;
}
