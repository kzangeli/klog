// 
// FILE            klHooks.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLHOOKS_H_
#define KLOG_KLHOOKS_H_

#include "klog/KlStatus.h"                   // KlStatus


// -----------------------------------------------------------------------------
//
// KlLogHook - function type for the Log Hook
//
// The Log Hook is a function that, if registered, is called once for each and every
// log line that passes through the conditions (trace level, verbose, etc)
//
typedef void (*KlLogHook)
(
  void*        vP,
  char*        text,
  char         type,
  const char*  file,
  int          lineNo,
  const char*  fName,
  int          tLev
);
  


// -----------------------------------------------------------------------------
//
// KlErrorHook - function type for the Error Hook
//
typedef void (*KlErrorHook)
(
  void*        vP,
  KlStatus     ks,
  const char*  errorDescription,
  const char*  file,
  int          lineNo,
  const char*  fName
);



// -----------------------------------------------------------------------------
//
// hooks -
//
extern KlLogHook    klLogHookF;    // hook function for all log messages - called once for each that pass
extern void*        klLogHookP;    // parameter for log hook function
extern KlErrorHook  klErrorHookF;  // hook function for errors
extern void*        klErrorHookP;  // parameter for error hook function



// -----------------------------------------------------------------------------
//
// KL_ERROR_HOOK - call error hook if configured
//
#define KL_ERROR_HOOK(status, description)                                             \
do                                                                                     \
{                                                                                      \
  if (klErrorHookF != NULL)                                                            \
    klErrorHookF(klErrorHookP, status, description, __FILE__, __LINE__, __FUNCTION__); \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_LOG_HOOK - call log hook if configured
//
#define KL_LOG_HOOK(ks)              \
do                                   \
{                                    \
  if (klLogHookF != NULL)            \
    klLogHookF(klLogHookP, ks);      \
} while (0)

#endif  // KLOG_KLHOOKS_H_
