// 
// FILE            klTest.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
//
// TEST DESCRIPTION
//   libklog offers to call a hook function for every error (and every log line as well).
//   This test program will use the error hook to make sure only programmed errors arrive.
//
//   xx. Trace Levels
//
//
//
//
#include <string.h>                          // memset, strncpy

#include "kbase/kTypes.h"                    // KBool
#include "kbase/kProgName.h"                 // kProgName
#include "kbase/kBasicLog.h"                 // KBL_*

#include "klog/kLog.h"                       // K Log Macros
#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klInit.h"                     // klInit
#include "klog/klConfig.h"                   // klConfig
#include "klog/klTraceLevelIsSet.h"          // klTraceLevelIsSet
#include "klog/klTraceLevelSet.h"            // klTraceLevelSet
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klComponentRegister.h"        // klComponentRegister
#include "klog/klComponentPresent.h"         // klComponentPresent
#include "klog/klComponentsPresent.h"        // klComponentsPresent
#include "klog/klComponentDestroy.h"         // klComponentDestroy
#include "klog/klDestroy.h"                  // klDestroy
#include "klog/klTraceLevelGet.h"            // klTraceLevelGet



// -----------------------------------------------------------------------------
//
// TEST_ASSERT - 
//
#define TEST_ASSERT(cond, r)                             \
do                                                       \
{                                                        \
  if (!(cond))                                           \
    KBL_RE(r, ("Condition (%s) not fulfilled", #cond));  \
} while (0)



// -----------------------------------------------------------------------------
//
// TEST_ASSERT_EQUAL
//
#define TEST_ASSERT_EQUAL(a, b, r)                               \
do                                                               \
{                                                                \
  if (a != b)                                                    \
    KBL_RE(r, ("Condition '%s' == '%s' not fulfilled", #a, #b)); \
} while (0)



// -----------------------------------------------------------------------------
//
// TEST_KLINIT -
//
#define TEST_KLINIT()                                  \
do                                                     \
{                                                      \
  ks = klInit(progName, "test/logs", KTRUE, NULL);     \
  if (ks != KlsOk)                                     \
    KBL_RE(1, ("klInit failed: %s", klStatus(ks)));    \
                                                       \
  ks = klConfig(NULL, KlcErrorHook, errorHook);        \
  if (ks != KlsOk)                                     \
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));         \
} while (0)



// -----------------------------------------------------------------------------
//
// TEST_KLEXIT -
//
#define TEST_KLEXIT()                                  \
do                                                     \
{                                                      \
  klDestroy();                                         \
} while (0)



// -----------------------------------------------------------------------------
//
// KSTATUS_CHECK -
//
#define KSTATUS_CHECK(expected, got, r)                                 \
do                                                                      \
{                                                                       \
  if (got != expected)                                                  \
    KBL_RE(r, ("Unexpected Status. Got %s (%d). Expected %s (%d)",      \
               klStatus(got),                                           \
               got,                                                     \
               klStatus(expected),                                      \
               expected));                                              \
} while (0);



// -----------------------------------------------------------------------------
//
// ERROR_HOOK_CHECK -
//
#define ERROR_HOOK_CHECK(expectedStatus, expectedString, r)             \
do                                                                      \
{                                                                       \
  if (errorHookStatus != expectedStatus)                                \
  {                                                                     \
    KBL_RE(r, ("Unexpected errorHookStatus: %s (%d). Expected %s (%d)", \
               klStatus(errorHookStatus),                               \
               errorHookStatus,                                         \
               klStatus(expectedStatus),                                \
               expectedStatus));                                        \
  }                                                                     \
                                                                        \
  if (strcmp(errorHookText, expectedString) != 0)                       \
  {                                                                     \
    KBL_RE(r+1, ("Unexpected errorHookText: '%s'. Expected '%s'",       \
                 errorHookText,                                         \
                 expectedString));                                      \
  }                                                                     \
} while (0);



// -----------------------------------------------------------------------------
//
// ERROR_HOOK_RESET - 
//
#define ERROR_HOOK_RESET()      \
do                              \
{                               \
  errorHookStatus = KlsOk;      \
  errorHookText   = "";         \
} while (0)



// -----------------------------------------------------------------------------
//
// COMP_NOT_NULL_CHECK -
//
#define COMP_NOT_NULL_CHECK(compP, r)             \
do                                                \
{                                                 \
  if (compP == NULL)                              \
    KBL_RE(r, ("klComponentRegister error"));     \
} while (0)



// -----------------------------------------------------------------------------
//
// COMP_IS_NULL_CHECK -
//
#define COMP_IS_NULL_CHECK(compP, r)              \
do                                                \
{                                                 \
  if (compP != NULL)                              \
    KBL_RE(r, ("Component should be NULL"));      \
} while (0)



// -----------------------------------------------------------------------------
//
// progName - 
//
char* progName = "not set";



// -----------------------------------------------------------------------------
//
// parse args vars
//
KBool silent = KFALSE;
KBool ktest  = KFALSE;



// -----------------------------------------------------------------------------
//
// errorHookStatus - status of last error reported to error hook function
//
KlStatus  errorHookStatus = KlsOk;
char*     errorHookText   = NULL;



// -----------------------------------------------------------------------------
//
// errorHook -
//
static void errorHook(void* vP, KlStatus ks, char* errorDescription, const char* file, int lineNo, const char* fName)
{
  KBL_V(("%s[%d]/errorHook: status %s (%d). error text: %s", file, lineNo, klStatus(ks), ks, errorDescription));
  errorHookStatus  = ks;
  errorHookText    = errorDescription;
}



// -----------------------------------------------------------------------------
//
// unitTests -
//
// UNIT TEST CASES
//
//   ut_klInit_tooLongLogFilePath                          - too long logFilePath length
//   ut_klInit_logDirectoryThatDoesntExist                 - klInit called with logDir "/no/dir"
//   ut_klInit_logDirectoryWithoutPermission               - klInit called with logDir that exists but without permission
//   ut_klInit_logFileThatExistsButWith444                 - klInit called with logDir/logFile that exists but with 444 permissions
//   ut_klInit_altLogFileNameTooLong                       -
//   ut_klInit_calledTwice                                 - call klInit twice, get an error on the second call
//
//   ut_klComponentRegister_klogNotInitialized             -
//   ut_klComponentRegister_tooManyTraceLevelsForComponent - too many trace levels for component
//   ut_klComponentRegister_outOfTraceLevels               - KlsOutOfTraceLevels
//   ut_klComponentRegister_tooManyComponents              - KlsNoSpaceForComponent
//   ut_klComponentRegister_componentDestruction           - klComponentDestroy
//   ut_klComponentRegister_alreadyExists                  - klComponentRegister
//
//   ut_klTraceLevelSet_twoErrors                          - invalid chars in trace level string
//   ut_traceLevelSet_manyErrors                           - 01. Component Offset > sizeof klTraceLevelV
//                                                         - 02. Invalid character in upper range
//                                                         - 03. Absurdly high single trace level (> sizeof traceLevelV)
//                                                         - 04. Components offet + single trace level > sizeof traceLevelV
//                                                         - 05. Attempt to set a components trace level above its registered maximum
//                                                         - 06. Range Error: From > To
//                                                         - 07. component offset "2-1": from > to
//                                                         - 08. Lower limit == K_VEC_SIZE(klTraceLevelV)
//                                                         - 09. component offset + lower limit >= sizeof klTraceLevelV
//                                                         - 10. component offset + upper limit >= sizeof klTraceLevelV
//                                                         - 11. Lower limit >= Registered number of trace levels
//                                                         - 12. Upper limit >= Registered number of trace levels
//                                                         - 13. component not found
//                                                         - 14. Invalid trace level string: '----'
//
// ut_klConfig_klogNotInitialized                          - call klConfig without having called klInit first
// ut_klConfig_invalidConfigItem                           - config item KlcInvalid and 90000
// ut_klConfig_invalidVerboseValue                         - use '2' as value for 'verbose'
// ut_klConfig_invalidFixmeValue                           - use '2' as value for 'fixme'
// ut_klConfig_noComponentAnywhere                         - NULL component and no component in trace levels
// ut_klConfig_invalidLogFileLineFormat                    - NULL log-file line-format
// ut_klConfig_invalidScreenLineFormat                     - NULL screen-log line-format
//
// ut_klTraceLevelIsSet_klogNotInitialized                 - klog not initialized
// ut_klTraceLevelIsSet_nullComp                           - NULL component
// ut_klTraceLevelIsSet_tlevelGtAbsSize                    - Not Implemented
// ut_klTraceLevelIsSet_tlevelAndCompOffsetGtAbsSize       - Not Implemented
// ut_klTraceLevelIsSet_tlevelGtCompLevels                 - Not Implemented
//
// ut_klTraceLevelGet_simple                               - just set a trace level and get the same string
//


// -----------------------------------------------------------------------------
//
// ut_klInit_tooLongLogFilePath - too long logFilePath length
//
int ut_klInit_tooLongLogFilePath(void)
{
  char*     progName = "ut_klInit_tooLongLogFilePath";
  KlStatus  ks;

  ks = klInit(progName,
              "/this/is/a/"
              "reeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
              "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
              "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
              "ally/long/path/for/the/log/file/directory", KTRUE, NULL);
  if (ks != KlsLogFilePathTooLong)
    KBL_RE(1, ("Real long logDir: %s", klStatus(ks)));

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klInit_logDirectoryThatDoesntExist - klInit called with logDir "/no/dir"
//
int ut_klInit_logDirectoryThatDoesntExist(void)
{
  char*     progName = "ut_klInit_logDirectoryThatDoesntExist";
  KlStatus  ks;

  ks = klInit(progName, "/no/dir", KTRUE, NULL);
  if (ks != KlsNoSuchDirectory)
    KBL_RE(1, ("logDir not a directory %s", klStatus(ks)));

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klInit_logDirectoryWithoutPermission - klInit called with logDir that exists but without permission
//
int ut_klInit_logDirectoryWithoutPermission(void)
{
  char*     progName = "ut_klInit_logDirectoryWithoutPermission";
  KlStatus  ks;

  ks = klInit(progName, "/usr/bin", KTRUE, NULL);
  if (ks != KlsNoWritePermissionsOnDirectory)
    KBL_RE(1, ("/usr/bin as logDir (no permissions there): %s", klStatus(ks)));

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klInit_logFileThatExistsButWith444 - klInit called with logDir/logFile that exists but with 444 permissions
//
int ut_klInit_logFileThatExistsButWith444(void)
{
  char*     progName = "ut_klInit_logFileThatExistsButWith444";
  KlStatus  ks;

  ks = klInit(progName, "./test/special", KTRUE, NULL);
  if (ks != KlsLogFileExistsButIsNotWritable)
    KBL_RE(1, ("logFile is test/klTest.log, which exists but is read-only: %s (chmod 444 test/special/klTest.log?)", klStatus(ks)));

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klInit_altLogFileNameTooLong - 
//
int ut_klInit_altLogFileNameTooLong(void)
{
  char*     progName = "ut_klInit_altLogFileNameTooLong";
  KlStatus  ks;

  ks = klInit(progName,
              NULL,
              KTRUE,
              "this_alternate_name_of_the _log_file_is_"
              "tooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo, "
              "faaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaar "
              "toooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo "
              "and some more toooooooooooooooooooooooooooooooooooooooooooooooo "
              "long");
  if (ks != KlsLogFilePathTooLong)
    KBL_RE(1, ("Real long alternative log file name: %s", klStatus(ks)));

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klInit_calledTwice - call klInit twice, get an error on the second call
//
int ut_klInit_calledTwice(void)
{
  char*     progName = "ut_klInit_calledTwice";
  KlStatus  ks;

  //
  // First call to klInit - also setting up Error Hook
  //
  TEST_KLINIT();

  //
  // Test case: second call to klInit must fail
  //
  errorHookStatus = KlsOk;
  ks = klInit(progName, NULL, KFALSE, NULL);
  KSTATUS_CHECK(KlsAlreadyInitialized, ks, 3);
  ERROR_HOOK_CHECK(KlsAlreadyInitialized, "klog library is already initialized", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klComponentRegister_klogNotInitialized - 
//
int ut_klComponentRegister_klogNotInitialized(void)
{
  KlComponent*  compP;

  compP = klComponentRegister("before klInit", 5, NULL);
  COMP_IS_NULL_CHECK(compP, 1);

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klComponentRegister_tooManyTraceLevelsForComponent - too many trace levels for component
//
int ut_klComponentRegister_tooManyTraceLevelsForComponent(void)
{
  char*         progName = "ut_klComponentRegister_tooManyTraceLevelsForComponent";
  KlComponent*  compP;
  KlStatus      ks;

  TEST_KLINIT();

  ERROR_HOOK_RESET();
  compP = klComponentRegister("greedy", KL_MAX_TRACE_LEVELS_PER_COMPONENT + 1, NULL);
  COMP_IS_NULL_CHECK(compP, 3);
  ERROR_HOOK_CHECK(KlsTooManyTraceLevelsForComponent, "number of trace levels for component exceeds the maximum allowed", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klComponentRegister_outOfTraceLevels - KlsOutOfTraceLevels
//
int ut_klComponentRegister_outOfTraceLevels(void)
{
  char*         progName = "ut_klComponentRegister_outOfTraceLevels";
  KlStatus      ks;
  KlComponent*  compP;

  TEST_KLINIT();

  // This is cheating: I set klTraceLevelOffset to a very high value ...
  klTraceLevelOffset = sizeof(klTraceLevelV) - 50;

  //
  // Now I try to register a component with 51 trace levels - there are only 50 left ...
  //
  compP = klComponentRegister("not accepted", 51, NULL);

  if (compP != NULL)
    KBL_RE(1, ("klComponentRegister accepted a comp with 51 trace levels although there are only 50 left!"));

  // With 50 it should be accepted
  compP = klComponentRegister("accepted", 50, NULL);
  if (compP == NULL)
    KBL_RE(2, ("klComponentRegister should accept a comp with 50 trace levels"));

  TEST_KLEXIT();

  return 0;  
}



// -----------------------------------------------------------------------------
//
// ut_klComponentRegister_tooManyComponents - KlsNoSpaceForComponent
//
int ut_klComponentRegister_tooManyComponents(void)
{
  char*         progName = "ut_klComponentRegister_tooManyComponents";
  KlStatus      ks;

  TEST_KLINIT();

  //
  // 13. register more components than the library admits, see error
  //
  KlComponent*  compP;
  int           ix;

  // First fill up the comp vector
  for (ix = 0; ix < KL_COMPONENTS_MAX; ix++)
  {
    char name[12];
    
    snprintf(name, sizeof(name), "comp%d", ix);
    compP = klComponentRegister(name, 100, NULL);
    if (compP != &klCompV[ix])
      KBL_RE(3, ("klComponentRegister: %p, should be %p for Component %d", compP, &klCompV[ix], ix));
  }
  compP = klComponentRegister("rejected", 100, NULL);
  if (compP != NULL)
    KBL_RE(4, ("klComponentRegister: comp %d should be rejected", ix));

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klComponentRegister_componentDestruction - klComponentDestroy
//
int ut_klComponentRegister_componentDestruction(void)
{
  char*         progName = "ut_klComponentRegister_componentDestruction";
  KlStatus      ks;
  KlComponent*  compP;
  
  TEST_KLINIT();

  compP = klComponentRegister("ok", 25, NULL);
  if (compP == NULL)
    KBL_RE(1, ("klComponentRegister error"));

  // Try to destroy a component, pointing at NULL (not a component)
  errorHookStatus = KlsOk;
  if ((ks = klComponentDestroy(NULL)) != KlsComponentNotFound)
    KBL_RE(3, ("klComponentDestroy(NULL): %s!", klStatus(ks)));
  if (errorHookStatus != KlsComponentNotFound)
    KBL_RE(4, ("erroneous errorHookStatus: %s", klStatus(errorHookStatus)));

  // Destroy the component
  if ((ks = klComponentDestroy(compP)) != KlsOk)
    KBL_RE(5, ("klComponentDestroy failed: %s", klStatus(ks)));

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klComponentRegister_alreadyExists - klComponentRegister
//
int ut_klComponentRegister_alreadyExists(void)
{
  char*         progName = "ut_klComponentRegister_alreadyExists";
  KlStatus      ks;
  KlComponent*  compP;
  
  TEST_KLINIT();

  compP = klComponentRegister("comp1", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));

  // Try to create a component with the same name ("comp1")
  errorHookStatus = KlsOk;
  if (klComponentRegister("comp1", 25, NULL) != NULL)
    KBL_RE(4, ("klComponentRegister: admitted to register a component with a name that is already in use"));
  if (errorHookStatus != KlsComponentAlreadyExists)
    KBL_RE(5, ("erroneous errorHookStatus: %s", klStatus(errorHookStatus)));

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klTraceLevelSet_twoErrors - klTraceLevelSet
//
// 2 errors to provoke:
//   01. klTraceLevelSet(NULL, NULL)
//   02. klTraceLevelSet(compP, "xyz")
//
int ut_klTraceLevelSet_twoErrors(void)
{
  char*         progName = "ut_klTraceLevelSet_twoErrors";
  KlStatus      ks;
  KlComponent*  compP;
  
  TEST_KLINIT();

  compP = klComponentRegister("ok", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));


  //
  // 01. klTraceLevelSet(NULL, NULL)
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(NULL, NULL);
  KSTATUS_CHECK(KlsInvalidParameter, ks, 4);
  ERROR_HOOK_CHECK(KlsInvalidParameter, "traceLevels parameter is NULL", 5);


  //
  // 02. klTraceLevelSet(compP, "xyz")
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "xyz");
  KSTATUS_CHECK(KlsInvalidCharacter, ks, 6);
  ERROR_HOOK_CHECK(KlsInvalidCharacter, "Invalid character in trace level parameter", 7);


  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_traceLevelSet_manyErrors -
//
// 12 errors to provoke:
//   01. Component Offset > sizeof klTraceLevelV
//   02. Invalid character in upper range
//   03. Absurdly high single trace level
//   04. Components offet + single trace level > sizeof traceLevelV
//   05. Attempt to set a components trace level above its registered maximum
//   06. Range Error: From > To
//   07. Off the charts FROM in range
//   08. Lower limit == K_VEC_SIZE(klTraceLevelV)
//   09. component offset + lower limit >= sizeof klTraceLevelV
//   10. component offset + upper limit >= sizeof klTraceLevelV
//   11. Lower limit >= Registered number of trace levels (25)
//   12. Upper limit >= Registered number of trace levels (25)
//   13. component not found
//   14. Invalid trace level string: '----'
//
int ut_traceLevelSet_manyErrors(void)
{
  char*         progName = "ut_traceLevelSet_manyErrors";
  KlStatus      ks;
  KlComponent*  compP;
  int           savedCompOffset;
  int           savedCompTLevs;

  TEST_KLINIT();

  compP = klComponentRegister("comp1", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));


  //
  // 01. Component Offset > sizeof klTraceLevelV
  //
  savedCompOffset = compP->offset;
  compP->offset = sizeof(klTraceLevelV);
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "1");
  KSTATUS_CHECK(KlsInvalidOffset, ks, 4);
  ERROR_HOOK_CHECK(KlsInvalidOffset, "component offset is off the charts", 5);
  compP->offset = savedCompOffset;


  //
  // 02. Invalid character in upper range
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "14--27");
  KSTATUS_CHECK(KlsInvalidCharacter, ks, 7);
  ERROR_HOOK_CHECK(KlsInvalidCharacter, "second hyphen found in trace level range", 8);


  //
  // 03. Absurdly high single trace level
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "250000");
  KSTATUS_CHECK(KlsInvalidTraceLevel, ks, 10);
  ERROR_HOOK_CHECK(KlsInvalidTraceLevel, "single value for trace level is off the charts", 11);


  //
  // 04. Components offet + single trace level > sizeof traceLevelV
  //
  ERROR_HOOK_RESET();
  savedCompOffset = compP->offset;
  savedCompTLevs  = compP->tLevs;
  compP->offset = sizeof(klTraceLevelV) - 2;
  compP->tLevs  = 1;
  ks = klTraceLevelSet(compP, "2");
  KSTATUS_CHECK(KlsInvalidOffset, ks, 13);
  ERROR_HOOK_CHECK(KlsInvalidOffset, "absolute trace level index for single value for trace level is off the charts", 14);
  compP->offset = savedCompOffset;
  compP->tLevs  = savedCompTLevs;

  //
  // 05. Attempt to set a components trace level above its registered maximum
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "25");  // The comp registered for 25 ("0-24")
  KSTATUS_CHECK(KlsInvalidOffsetForComponent, ks, 16);
  ERROR_HOOK_CHECK(KlsInvalidOffsetForComponent, "single value for trace level exceeds the number of trace levels registered for component", 17);


  //
  // 06. Range Error: From > To
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "2-1");
  KSTATUS_CHECK(KlsInvalidTraceLevel, ks, 19);
  ERROR_HOOK_CHECK(KlsInvalidTraceLevel, "lower limit for trace level range exceeds the upper limit", 20);
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "2-2");  // lower limit == upper limit is OK
  KSTATUS_CHECK(KlsOk, ks, 22);
  ERROR_HOOK_CHECK(KlsOk, "", 23);


  //
  // 07. Off the charts FROM in range
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "250000-250001");
  KSTATUS_CHECK(KlsInvalidTraceLevel, ks, 25);
  ERROR_HOOK_CHECK(KlsInvalidTraceLevel, "lower limit for trace level range is off the charts", 26);


  //
  // 08. Lower limit == K_VEC_SIZE(klTraceLevelV)
  //
  ERROR_HOOK_RESET();
  char cV[32];
  snprintf(cV, sizeof(cV), "%lu-%lu", K_VEC_SIZE(klTraceLevelV), K_VEC_SIZE(klTraceLevelV) + 1);
  ks = klTraceLevelSet(compP, cV);
  KSTATUS_CHECK(KlsInvalidTraceLevel, ks, 28);
  ERROR_HOOK_CHECK(KlsInvalidTraceLevel, "lower limit for trace level range is off the charts", 29);
  

  //
  // 09. component offset + lower limit >= sizeof klTraceLevelV
  //     Can't use the first component here as its offset is 0 ... need another component
  //     The first component would just get KlsInvalidOffset ...
  //     This second component will have its offset == 25 (as comp1 asks for 25 tracelevels)
  //
  KlComponent* comp2P = klComponentRegister("comp2", 25, NULL);  
  TEST_ASSERT(comp2P != NULL, 31);
  TEST_ASSERT_EQUAL(comp2P->offset, 25, 32);
  ERROR_HOOK_RESET();
  int limitValue = K_VEC_SIZE(klTraceLevelV) - comp2P->offset + 1;
  snprintf(cV, sizeof(cV), "%d-%d", limitValue, limitValue + 1);
  ks = klTraceLevelSet(comp2P, cV);
  KSTATUS_CHECK(KlsInvalidOffset, ks, 31);
  ERROR_HOOK_CHECK(KlsInvalidOffset, "absolute trace level index of lower limit for trace level range is off the charts", 32);


  //
  // 10. component offset + upper limit >= sizeof klTraceLevelV
  //
  // comp2P from previous test is used, starting on offset 25. For the same reason as 09.
  //
  ERROR_HOOK_RESET();
  snprintf(cV, sizeof(cV), "0-%d", limitValue + 1);
  ks = klTraceLevelSet(comp2P, cV);
  KSTATUS_CHECK(KlsInvalidOffset, ks, 34);
  ERROR_HOOK_CHECK(KlsInvalidOffset, "absolute trace level index of upper limit for trace level range is off the charts", 35);


  //
  // 11. Lower limit >= Registered number of trace levels (25)
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "25-26");
  KSTATUS_CHECK(KlsInvalidOffsetForComponent, ks, 36);
  ERROR_HOOK_CHECK(KlsInvalidOffsetForComponent, "lower limit for trace level range exceeds the number of trace levels registered for component", 37);


  //
  // 12. Upper limit >= Registered number of trace levels (25)
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "20-25");
  KSTATUS_CHECK(KlsInvalidOffsetForComponent, ks, 39);
  ERROR_HOOK_CHECK(KlsInvalidOffsetForComponent, "upper limit for trace level range exceeds the number of trace levels registered for component", 40);


  //
  // 13. component not found
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(NULL, "no_comp:0-4");
  KSTATUS_CHECK(KlsComponentNotFound, ks, 45);
  ERROR_HOOK_CHECK(KlsComponentNotFound, "component in trace level string was not found", 46);

  //
  // All types of errors in trace level string: '----', '-,', ',,,', '-2a'
  //

  //
  // 14. Invalid trace level string: '----'
  //
  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, "----");
  KSTATUS_CHECK(KlsInvalidCharacter, ks, 48);
  ERROR_HOOK_CHECK(KlsInvalidCharacter, "second hyphen found in trace level range", 49);


  //
  // Done!
  //
  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_klogNotInitialized - call klConfig without having called klInit first
//
int ut_klConfig_klogNotInitialized(void)
{
  KlStatus   ks;

  // 00. Call klConfig without having called klInit first
  ks = klConfig(NULL, KlcInvalid, NULL);
  KSTATUS_CHECK(ks, KlsNotInitialized, 1);
  // No hook check possible, as the lib isn't even initialized ...

  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_invalidConfigItem - config item KlcInvalid and 90000
//
int ut_klConfig_invalidConfigItem(void)
{
  char*         progName = "ut_klConfig_invalidConfigItem";
  KlStatus      ks;

  TEST_KLINIT();

  ERROR_HOOK_RESET();
  ks = klConfig(NULL, KlcInvalid, NULL);
  KSTATUS_CHECK(ks, KlsInvalidConfigItem, 3);
  ERROR_HOOK_CHECK(KlsInvalidConfigItem, "invalid config item", 5);

  ERROR_HOOK_RESET();
  ks = klConfig(NULL, (KlConfigItem) 90000, NULL);
  KSTATUS_CHECK(ks, KlsInvalidConfigItem, 3);
  ERROR_HOOK_CHECK(KlsInvalidConfigItem, "invalid config item II", 5);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_invalidVerboseValue - use '2' as value for 'verbose'
//
int ut_klConfig_invalidVerboseValue(void)
{
  char*         progName = "ut_klConfig_invalidVerboseValue";
  KlStatus      ks;

  TEST_KLINIT();
  ERROR_HOOK_RESET();
  ks = klConfig(NULL, KlcVerbose, (void*) 2);
  KSTATUS_CHECK(ks, KlsInvalidParameter, 3);
  ERROR_HOOK_CHECK(KlsInvalidParameter, "bad value for a boolean (must be 1 or 0)", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_invalidFixmeValue - use '2' as value for 'fixme'
//
int ut_klConfig_invalidFixmeValue(void)
{
  char*         progName = "ut_klConfig_invalidFixmeValue";
  KlStatus      ks;

  TEST_KLINIT();
  ERROR_HOOK_RESET();
  ks = klConfig(NULL, KlcFixme, (void*) 2);
  KSTATUS_CHECK(ks, KlsInvalidParameter, 3);
  ERROR_HOOK_CHECK(KlsInvalidParameter, "bad value for a boolean (must be 1 or 0)", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_noComponentAnywhere - NULL component
//
int ut_klConfig_noComponentAnywhere(void)
{
  char*         progName = "ut_klConfig_noComponentAnywhere";
  KlStatus      ks;

  TEST_KLINIT();
  ERROR_HOOK_RESET();
  KBL_V(("****** ut_klConfig_noComponentAnywhere calling klConfig"));
  ks = klConfig(NULL, KlcTraceLevelSet, "0-14");
  KBL_V(("****** Back in ut_klConfig_noComponentAnywhere. ks == %d (%s)", ks, klStatus(ks)));
  KSTATUS_CHECK(ks, KlsNoComponentName, 3);
  ERROR_HOOK_CHECK(KlsNoComponentName, "no colon found in trace-level string => no component name found", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_invalidLogFileLineFormat - NULL log-file line-format
//
int ut_klConfig_invalidLogFileLineFormat(void)
{
  char*         progName = "ut_klConfig_invalidLogFileLineFormat";
  KlStatus      ks;

  TEST_KLINIT();
  ERROR_HOOK_RESET();
  ks = klConfig(NULL, KlcLogFileLineFormat, NULL);
  KSTATUS_CHECK(ks, KlsInvalidParameter, 3);
  ERROR_HOOK_CHECK(KlsInvalidParameter, "invalid parameter", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klConfig_invalidScreenLineFormat - NULL screen-log line-format
//
int ut_klConfig_invalidScreenLineFormat(void)
{
  char*         progName = "ut_klConfig_invalidScreenLineFormat";
  KlStatus      ks;

  TEST_KLINIT();
  ERROR_HOOK_RESET();
  ks = klConfig(NULL, KlcScreenLineFormat, NULL);
  KSTATUS_CHECK(ks, KlsInvalidParameter, 3);
  ERROR_HOOK_CHECK(KlsInvalidParameter, "invalid parameter", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klTraceLevelIsSet_klogNotInitialized - call klTraceLevelIsSet without calling klInit first
//
int ut_klTraceLevelIsSet_klogNotInitialized(void)
{
  KBool set;

  set = klTraceLevelIsSet(NULL, 4);
  if (set != KFALSE)
    return 1;

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klTraceLevelIsSet_nullComp
//
int ut_klTraceLevelIsSet_nullComp(void)
{
  KBool     set;
  KlStatus  ks;

  TEST_KLINIT();

  ERROR_HOOK_RESET();
  set = klTraceLevelIsSet(NULL, 4);
  KSTATUS_CHECK(KFALSE, set, 3);
  ERROR_HOOK_CHECK(KlsInvalidParameter, "Component is NULL", 4);

  TEST_KLEXIT();
  return 0;
}



// -----------------------------------------------------------------------------
//
// ut_klTraceLevelGet_simple
//
int ut_klTraceLevelGet_simple(void)
{
  char*         progName = "ut_klTraceLevelGet_simple";
  KlComponent*  compP;
  KlStatus      ks;
  char*         traceLevels = "0,2,5,7-11,19,21-24";

  TEST_KLINIT();

  compP = klComponentRegister("ok", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));

  ERROR_HOOK_RESET();
  ks = klTraceLevelSet(compP, traceLevels);
  KSTATUS_CHECK(KlsOk, ks, 4);
  ERROR_HOOK_CHECK(KlsOk, "", 5);

  char out[64];
  ks = klTraceLevelGet(compP, out, sizeof(out));
  KSTATUS_CHECK(KlsOk, ks, 7);
  ERROR_HOOK_CHECK(KlsOk, "", 8);
  
  if (strcmp(out, traceLevels) != 0)
  {
    KBL_E(("Expected: '%s'", traceLevels));
    KBL_E(("Got:      '%s'", out));

    return 10;
  }

  TEST_KLEXIT();
  return 0;
}



// ut_klTraceLevelIsSet_tlevelGtAbsSize
// ut_klTraceLevelIsSet_tlevelAndCompOffsetGtAbsSize
// ut_klTraceLevelIsSet_tlevelGtCompLevels



#define DOT_LINE_LEN 120
// -----------------------------------------------------------------------------
//
// TEST - run one test (unit or func test)
//
#define TEST(tNo, tCall, tName)                                 \
do                                                              \
{                                                               \
  int  s;                                                       \
  char dots[DOT_LINE_LEN + 1];                                  \
                                                                \
  memset(dots, '.', sizeof(dots) - 1);                          \
  dots[DOT_LINE_LEN] = 0;                                       \
  strncpy(dots, tName, strlen(tName));                          \
  dots[strlen(tName)] = ' ';                                    \
  if ((s = tCall()) != 0)                                       \
    printf("%03d. %s failed (error %d)\n", tNo, dots, s);       \
  else                                                          \
    printf("%03d. %s OK\n", tNo, dots);                         \
} while (0)



// -----------------------------------------------------------------------------
//
// unitTests -
//
int unitTests(int id)
{
  if ((id <= 0) || (id == 1))   TEST(1,  ut_klInit_tooLongLogFilePath,                          "klInit: too long log file path");
  if ((id <= 0) || (id == 2))   TEST(2,  ut_klInit_logDirectoryThatDoesntExist,                 "klInit: log directory that doesn't exist");
  if ((id <= 0) || (id == 3))   TEST(3,  ut_klInit_logDirectoryWithoutPermission,               "klInit: log directory without permission");
  if ((id <= 0) || (id == 4))   TEST(4,  ut_klInit_logFileThatExistsButWith444,                 "klInit: logFile that exists but with 444");
  if ((id <= 0) || (id == 5))   TEST(5,  ut_klInit_altLogFileNameTooLong,                       "klInit: param 4 (alt log file name) too long");
  if ((id <= 0) || (id == 6))   TEST(6,  ut_klInit_calledTwice,                                 "klInit: called twice");
  if ((id <= 0) || (id == 11))  TEST(11, ut_klComponentRegister_klogNotInitialized,             "klComponentRegister: klog not initialized");
  if ((id <= 0) || (id == 12))  TEST(12, ut_klComponentRegister_tooManyTraceLevelsForComponent, "klComponentRegister: too many trace levels for component");
  if ((id <= 0) || (id == 13))  TEST(13, ut_klComponentRegister_outOfTraceLevels,               "klComponentRegister: out of trace levels");
  if ((id <= 0) || (id == 14))  TEST(14, ut_klComponentRegister_tooManyComponents,              "klComponentRegister: too many components");
  if ((id <= 0) || (id == 15))  TEST(15, ut_klComponentRegister_componentDestruction,           "klComponentRegister: component destruction");
  if ((id <= 0) || (id == 16))  TEST(16, ut_klComponentRegister_alreadyExists,                  "klComponentRegister: component registered twice");
  if ((id <= 0) || (id == 21))  TEST(21, ut_klTraceLevelSet_twoErrors,                          "klTraceLevelSet: 2 errors");
  if ((id <= 0) || (id == 22))  TEST(22, ut_traceLevelSet_manyErrors,                           "traceLevelSet: many errors");
  if ((id <= 0) || (id == 31))  TEST(31, ut_klConfig_klogNotInitialized,                        "klConfig:  klog not initialized");
  if ((id <= 0) || (id == 32))  TEST(32, ut_klConfig_invalidConfigItem,                         "klConfig: invalid config item");
  if ((id <= 0) || (id == 33))  TEST(33, ut_klConfig_invalidVerboseValue,                       "klConfig: invalid value for KlcVerbose");
  if ((id <= 0) || (id == 34))  TEST(34, ut_klConfig_invalidFixmeValue,                         "klConfig: invalid value for KlcFixme");
  if ((id <= 0) || (id == 35))  TEST(35, ut_klConfig_noComponentAnywhere,                       "klConfig: invalid value (comp is NULL and no comp in trace level) for KlcTraceLevelSet");
  if ((id <= 0) || (id == 36))  TEST(36, ut_klConfig_invalidLogFileLineFormat,                  "klConfig: invalid value (NULL) for KlcLogFileLineFormat");
  if ((id <= 0) || (id == 37))  TEST(37, ut_klConfig_invalidScreenLineFormat,                   "klConfig: invalid value (NULL) for KlcScreenLineFormat");

  if ((id <= 0) || (id == 41))  TEST(41, ut_klTraceLevelIsSet_klogNotInitialized,               "klTraceLevelIsSet: klog not initialized");
  if ((id <= 0) || (id == 42))  TEST(42, ut_klTraceLevelIsSet_nullComp,                         "klTraceLevelIsSet: NULL component");

  if ((id <= 0) || (id == 51))  TEST(51, ut_klTraceLevelGet_simple,                             "klTraceLevelGet:   simple set/get/check");

  return 0;  // All OK
}



// -----------------------------------------------------------------------------
//
// funcTest01 - assure that the log file is actually created under default conditions
//
int funcTest01(void)
{
  KlStatus      ks;
  KlComponent*  compP;
  
  ks = klInit(progName, NULL, KTRUE, NULL);
  if (ks != KlsOk)
    KBL_X(1, ("klInit failed: %s", klStatus(ks)));
  
  ks = klConfig(NULL, KlcErrorHook, errorHook);
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  ks = klConfig(NULL, KlcLogFileLineFormat, "TYPE: COMP:FILE[0]:FUNC: MSG");
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  errorHookStatus = KlsOk;
  compP = klComponentRegister("ok", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));

  KL_M(compP, ("funcTest01 OK"));

  if (errorHookStatus != KlsOk)
    KBL_RE(4, ("errorHookStatus == %s", klStatus(errorHookStatus)));
  
  KBL_M(("funcTest01 - assure that the log file is actually created under default conditions"));

  return 0;
}



// -----------------------------------------------------------------------------
//
// funcTest02 - assure that the log file is correctly created in non-default directory
//
int funcTest02(void)
{
  KlStatus      ks;
  KlComponent*  compP;

  ks = klInit(progName, "test/logs", KTRUE, NULL);
  if (ks != KlsOk)
    KBL_X(1, ("klInit failed: %s", klStatus(ks)));
  
  ks = klConfig(NULL, KlcErrorHook, errorHook);
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  ks = klConfig(NULL, KlcLogFileLineFormat, "TYPE: COMP:FILE[0]:FUNC: MSG");
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  errorHookStatus = KlsOk;
  compP = klComponentRegister("ok", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));

  if (errorHookStatus != KlsOk)
    KBL_RE(4, ("errorHookStatus == %s", klStatus(errorHookStatus)));
  
  KBL_M(("funcTest02 - assure that the log file is correctly created as test/logs/klTest.log"));

  return 0;
}



// -----------------------------------------------------------------------------
//
// funcTest03 - assure that the log file is correctly created with alternative name
//
int funcTest03(void)
{
  KlStatus      ks;
  KlComponent*  compP;

  ks = klInit(progName, "test/logs", KTRUE, "funcTest03_altname");
  if (ks != KlsOk)
    KBL_X(1, ("klInit failed: %s", klStatus(ks)));

  ks = klConfig(NULL, KlcErrorHook, errorHook);
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  ks = klConfig(NULL, KlcLogFileLineFormat, "TYPE: COMP:FILE[0]:FUNC: MSG");
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  errorHookStatus = KlsOk;
  compP = klComponentRegister("ok", 25, NULL);
  if (compP == NULL)
    KBL_RE(3, ("klComponentRegister error"));

  if (errorHookStatus != KlsOk)
    KBL_RE(4, ("errorHookStatus == %s", klStatus(errorHookStatus)));
  
  KBL_M(("funcTest03 - assure that the log file is correctly created as test/logs/funcTest03_altname.log"));

  return 0;
}




// -----------------------------------------------------------------------------
//
// funcTest04 - create 3 log components, set their log levels, and send logs
//
int funcTest04(void)
{
  KlStatus      ks;
  KlComponent*  comp1;
  KlComponent*  comp2;
  KlComponent*  comp3;
  
  ks = klInit(progName, NULL, KTRUE, NULL);
  if (ks != KlsOk)
    KBL_X(1, ("klInit failed: %s", klStatus(ks)));
  
  ks = klConfig(NULL, KlcErrorHook, errorHook);
  if (ks != KlsOk)
    KBL_RE(2, ("klConfig: %s", klStatus(ks)));

  ks = klConfig(NULL, KlcLogFileLineFormat, "TYPE: COMP:FILE[0]:FUNC: MSG");
  if (ks != KlsOk)
    KBL_RE(3, ("klConfig: %s", klStatus(ks)));

  errorHookStatus = KlsOk;
  comp1 = klComponentRegister("comp1", 25, NULL);
  if (comp1 == NULL)
    KBL_RE(4, ("klComponentRegister error"));
  if (errorHookStatus != KlsOk)
    KBL_RE(5, ("errorHookStatus == %s", klStatus(errorHookStatus)));

  errorHookStatus = KlsOk;
  comp2 = klComponentRegister("comp2", 25, NULL);
  if (comp2 == NULL)
    KBL_RE(6, ("klComponentRegister error"));
  if (errorHookStatus != KlsOk)
    KBL_RE(7, ("errorHookStatus == %s", klStatus(errorHookStatus)));

  errorHookStatus = KlsOk;
  comp3 = klComponentRegister("comp3", 25, NULL);
  if (comp3 == NULL)
    KBL_RE(8, ("klComponentRegister error"));
  if (errorHookStatus != KlsOk)
    KBL_RE(9, ("errorHookStatus == %s", klStatus(errorHookStatus)));


  // 
  // One forced log line per component
  // 
  KL_M(comp1, ("funcTest04 - comp1 forced log line"));
  KL_M(comp2, ("funcTest04 - comp2 forced log line"));
  KL_M(comp3, ("funcTest04 - comp3 forced log line"));

  
  //
  // one verbose log line per component - but verbose is OFF for all three components
  //
  KL_V(comp1, ("funcTest04 - comp1 verbose log line - should not be seen!!!"));
  KL_V(comp2, ("funcTest04 - comp2 verbose log line - should not be seen!!!"));
  KL_V(comp3, ("funcTest04 - comp3 verbose log line - should not be seen!!!"));

  //
  // Set verbose for all three components
  // 
  ks = klConfig(comp1, KlcVerbose, (void*) KTRUE);
  if (ks != KlsOk)
    KBL_RE(10, ("klConfig(comp1, KlcVerbose): %s", klStatus(ks)));
  
  ks = klConfig(comp2, KlcVerbose, (void*) KTRUE);
  if (ks != KlsOk)
    KBL_RE(11, ("klConfig(comp2, KlcVerbose): %s", klStatus(ks)));

  ks = klConfig(comp3, KlcVerbose, (void*) KTRUE);
  if (ks != KlsOk)
    KBL_RE(12, ("klConfig(comp3, KlcVerbose): %s", klStatus(ks)));

  //
  // one verbose log line per component - now verbose is ON for all three components
  //
  KL_V(comp1, ("funcTest04 - comp1 verbose log line - should be seen!!!"));
  KL_V(comp2, ("funcTest04 - comp2 verbose log line - should be seen!!!"));
  KL_V(comp3, ("funcTest04 - comp3 verbose log line - should be seen!!!"));




  //
  // one fixme log line per component - but fixme is OFF for all three components
  //
  KL_FIXME(comp1, 1, ("funcTest04 - comp1 fixme log line - should not be seen!!!"));
  KL_FIXME(comp2, 2, ("funcTest04 - comp2 fixme log line - should not be seen!!!"));
  KL_FIXME(comp3, 3, ("funcTest04 - comp3 fixme log line - should not be seen!!!"));

  //
  // Set fixme for all three components
  // 
  ks = klConfig(comp1, KlcFixme, (void*) KTRUE);
  if (ks != KlsOk)
    KBL_RE(10, ("klConfig(comp1, KlcFixme): %s", klStatus(ks)));
  
  ks = klConfig(comp2, KlcFixme, (void*) KTRUE);
  if (ks != KlsOk)
    KBL_RE(11, ("klConfig(comp2, KlcFixme): %s", klStatus(ks)));

  ks = klConfig(comp3, KlcFixme, (void*) KTRUE);
  if (ks != KlsOk)
    KBL_RE(12, ("klConfig(comp3, KlcFixme): %s", klStatus(ks)));

  //
  // one fixme log line per component - now fixme is ON for all three components
  //
  KL_FIXME(comp1, 1, ("funcTest04 - comp1 fixme log line - should be seen!!!"));
  KL_FIXME(comp2, 2, ("funcTest04 - comp2 fixme log line - should be seen!!!"));
  KL_FIXME(comp3, 3, ("funcTest04 - comp3 fixme log line - should be seen!!!"));



  //
  // Now individual trace levels ...
  // - set trace levels "0-4,20-24" for comp1
  // - set trace levels "10-14,18"  for comp2
  // - set trace levels "24"        for comp3
  //
  // Then issue KL_T for trace levels 0-24 for all three components - see 17 lines in log file
  //
  ks = klConfig(comp1, KlcTraceLevelSet, "0-4,20-24");
  if (ks != KlsOk)
    KBL_RE(13, ("klConfig(comp1, KlcTraceLevelSet): %s", klStatus(ks)));

  ks = klConfig(comp2, KlcTraceLevelSet, "10-14,18");
  if (ks != KlsOk)
    KBL_RE(14, ("klConfig(comp2, KlcTraceLevelSet): %s", klStatus(ks)));
  
  ks = klConfig(comp3, KlcTraceLevelSet, "24");
  if (ks != KlsOk)
    KBL_RE(13, ("klConfig(comp3, KlcTraceLevelSet): %s", klStatus(ks)));

  int tLev;
  for (tLev = 0; tLev < 25; tLev++)
  {
    KL_T(comp1, tLev, ("Component 1, this is trace level %d", tLev));
    KL_T(comp2, tLev, ("Component 2, this is trace level %d", tLev));
    KL_T(comp3, tLev, ("Component 3, this is trace level %d", tLev));
  }
  

  KL_M(comp1, ("Removing trace levels"));
  // Now remove all traceLevels and run tLev-loop again - nothing to log-file
  ks = klConfig(NULL, KlcTraceLevelSet, "comp1:None;comp2:None;comp3:None");
  if (ks != KlsOk)
    KBL_RE(14, ("klConfig(comp1:None;comp2:None;comp3:None): %s", klStatus(ks)));
  
  for (tLev = 0; tLev < 25; tLev++)
  {
    KL_T(comp1, tLev, ("Component 1, this is trace level %d - should NOT BE SEEN", tLev));
    KL_T(comp2, tLev, ("Component 2, this is trace level %d - should NOT BE SEEN", tLev));
    KL_T(comp3, tLev, ("Component 3, this is trace level %d - should NOT BE SEEN", tLev));
  }
  
  char* traceLevels = "comp1:1,2,3-5;comp2:10-12,13,14-15;comp3:20,21,22-24";
  KL_M(comp1, ("Setting new trace levels: %s", traceLevels));
  ks = klConfig(NULL, KlcTraceLevelSet, traceLevels);
  if (ks != KlsOk)
    KBL_RE(14, ("klConfig(KlcTraceLevelSet, '%s'): %s", traceLevels, klStatus(ks)));

  KL_M(comp1, ("16 trace lines should be visible under this line")); 
  for (tLev = 0; tLev < 25; tLev++)
  {
    KL_T(comp1, tLev, ("Component 1, this is trace level %d", tLev));
    KL_T(comp2, tLev, ("Component 2, this is trace level %d", tLev));
    KL_T(comp3, tLev, ("Component 3, this is trace level %d", tLev));
  }


  //
  // Now that we have three components with different trace levels, lets use that and test klTraceLevelGet also
  //
  char tlv[64];

  ks = klTraceLevelGet(comp1, tlv, sizeof(tlv));
  if (ks != KlsOk)
    KBL_RE(15, ("klTraceLevelGet(comp1): %s", klStatus(ks)));
  if (strcmp(tlv, "1-5") != 0)
    KBL_RE(16, ("klTraceLevelGet(comp1): got '%s', expected '%s'", tlv, "1-5"));
  KL_M(comp1, ("Trace levels for comp1 are: '%s'", tlv));
  
  ks = klTraceLevelGet(comp2, tlv, sizeof(tlv));
  if (ks != KlsOk)
    KBL_RE(15, ("klTraceLevelGet(comp2): %s", klStatus(ks)));
  if (strcmp(tlv, "10-15") != 0)
    KBL_RE(16, ("klTraceLevelGet(comp2): got '%s', expected '%s'", tlv, "10-15"));
  KL_M(comp1, ("Trace levels for comp2 are: '%s'", tlv));

  ks = klTraceLevelGet(comp3, tlv, sizeof(tlv));
  if (ks != KlsOk)
    KBL_RE(15, ("klTraceLevelGet(comp3): %s", klStatus(ks)));
  if (strcmp(tlv, "20-24") != 0)
    KBL_RE(16, ("klTraceLevelGet(comp3): got '%s', expected '%s'", tlv, "20-24"));
  KL_M(comp1, ("Trace levels for comp3 are: '%s'", tlv));

  
  //
  // And, lets ADD some trace levels for comp1, se we see some commas in the retrieved trace level as well
  //
  ks = klTraceLevelSet(comp1, "7,9-12,15,17,21-23");
  if (ks != KlsOk)
    KBL_RE(14, ("klConfig(KlcTraceLevelSet, '%s'): %s", "7,9-12,15,17,21-23", klStatus(ks)));

  // Now get (and check) the total trace level string for comp1
  ks = klTraceLevelGet(comp1, tlv, sizeof(tlv));
  if (ks != KlsOk)
    KBL_RE(15, ("klTraceLevelGet(comp1): %s", klStatus(ks)));
  if (strcmp(tlv, "1-5,7,9-12,15,17,21-23") != 0)
    KBL_RE(16, ("klTraceLevelGet(comp1): got '%s', expected '%s'", tlv, "1-5,7,9-12,15,17,21-23"));
  KL_M(comp1, ("Trace levels for comp1 are: '%s'", tlv));
  

  //
  // Test Done
  //
  KBL_M(("funcTest04 - assure that the log file contains all the log lines issued by this function (51 lines)"));

  return 0;
}



// -----------------------------------------------------------------------------
//
// funcTests - 
//
int funcTests(int id)
{
  int r = 0;

  if (ktest == KFALSE)  // manual tests
  {
    if      (id == 1)  TEST(1, funcTest01, "funcTest01");
    else if (id == 2)  TEST(2, funcTest02, "funcTest02");
    else if (id == 3)  TEST(3, funcTest03, "funcTest03");
    else if (id == 4)  TEST(4, funcTest04, "funcTest04");
  }
  else  // automated test
  {
    if      (id == 1)  r = funcTest01();
    else if (id == 2)  r = funcTest02();
    else if (id == 3)  r = funcTest03();
    else if (id == 4)  r = funcTest04();
  }

  return r;
}



// -----------------------------------------------------------------------------
//
// usage -
//
void usage(char* progName, int exitCode)
{
  printf("Usage: %s -u (show this message)\n", progName);
  printf("Usage: %s [-v (verbose)] [-ktest (running under ktest)] [-ut (unit test index - 0 for all unit tests] [-ft (func test index - 0 for all func tests] [-silent (no message per test for functests)] \n", progName);

  exit(exitCode);
}



// -----------------------------------------------------------------------------
//
// main -
//
int main(int argC, char* argV[])
{
  int   ut = -1;
  int   ft = -1;

  progName = kProgName(argV[0]);


  //
  // parse args
  //
  int ix = 1;
  while (ix < argC)
  {
    if (strcmp(argV[ix], "-u") == 0)
      usage(argV[0], 1);
    else if (strcmp(argV[ix], "-v") == 0)
      kbVerbose = KTRUE;
    else if (strcmp(argV[ix], "--ktest") == 0)
      ktest = KTRUE;
    else if (strcmp(argV[ix], "-silent") == 0)
    {
      kbVerbose = KFALSE;
      silent     = KTRUE;
    }
    else if (strcmp(argV[ix], "-ut") == 0)
    {
      ++ix;

      if (ix >= argC)
        KBL_X(1, ("-ut expects an integer"));

      ut = atoi(argV[ix]);
    }
    else if (strcmp(argV[ix], "--unitTests") == 0)
    {
      ut = 0;
    }
    else if (strcmp(argV[ix], "-ft") == 0)
    {
      ++ix;

      if (ix >= argC)
        KBL_X(1, ("-ft expects an integer"));
      
      ft = atoi(argV[ix]);
    }
    
    ++ix;
  }



  //
  // Line numbers in log file often change and make the diff more complicated.
  // So, for 'ktest' tests, line numbers are switched off - always shown as ZERO.
  //
  // Also, silent mode is required for ktest runs.
  //
  if (ktest == KTRUE)
  {
    kbNoLineNumbers = KTRUE;
    kbVerbose       = KFALSE;
    silent          = KTRUE;
  }

  if ((ut != -1) && (ft != -1))
  {
    KBL_M(("CLI error: can't use -ut and -ft together"));
    usage(argV[0], 2);
  }
  else if ((ut == -1) && (ft == -1))
  {
    // No ut/ft CLI given => ALL unit tests

    int id;
    
    if ((id = unitTests(ut)) != 0)
      KBL_X(1, ("error in unit test %d", id));

    printf("All unit tests OK\n");
    exit(0);
  }
  
  if (ut != -1)
  {
    int id;

    if ((id = unitTests(ut)) != 0)
      KBL_X(1, ("error in unit test %d", id));

    printf("All Unit Tests OK\n");
    exit(0);
  }

  if (ft != -1)
  {
    int id;

    if ((id = funcTests(ft)) != 0)
      KBL_X(1, ("error in func test %d", id));

    printf("Func Test OK\n");
    exit(0);
   }
  
  return 0;
}
