// 
// FILE            klConfig.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stddef.h>                          // NULL
#include <string.h>                          // strspn
#include <stdlib.h>                          // free

#include "kbase/kTypes.h"                    // KBool
#include "kbase/kMacros.h"                   // K_VEC_SIZE, K_FT
#include "kbase/kBasicLog.h"                 // Basic log macros using printf

#include "klog/klInit.h"                     // KL_INITALIZED_CHECK, klScreenFd
#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klHooks.h"                    // hook functionality
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klConfigVars.h"               // klLogFileFormat, klScreenFormat
#include "klog/klTraceLevelSet.h"            // klTraceLevelSet
#include "klog/klVerboseLevelSet.h"          // klVerboseLevelSet
#include "klog/klConfig.h"                   // Own interface



// -----------------------------------------------------------------------------
//
// NULL_CHECK - check variable for NULL
//
#define NULL_CHECK(p)                                         \
do                                                            \
{                                                             \
  if (p == NULL)                                              \
  {                                                           \
    KL_ERROR_HOOK(KlsInvalidParameter, "invalid parameter");  \
    return KlsInvalidParameter;                               \
  }                                                           \
} while (0)



// -----------------------------------------------------------------------------
//
// Screen output File Descriptor
//
int klScreenFd = -1;



// -----------------------------------------------------------------------------
//
// klConfig -
//
KlStatus klConfig(KlComponent* compP, KlConfigItem item, void* vP)
{
  KBool    b;
  KlStatus ks = KlsOk;

  KL_INITALIZED_CHECK(KlsNotInitialized);

  if ((vP == (void*) KTRUE) || (vP == (void*) KFALSE))
  {
    b = (vP == (void*) KTRUE)? KTRUE : KFALSE;
    vP = &b;
  }

  //
  // if compP == NULL, the configuration applies to ALL components
  //

  switch (item)
  {
  case KlcErrorsToStderr:
    klErrorsToStderr = *((KBool*) vP);
    return KlsOk;

  case KlcWarningsToStderr:
    klWarningsToStderr = *((KBool*) vP);
    return KlsOk;

  case KlcInvalid:
    KL_ERROR_HOOK(KlsInvalidConfigItem, "invalid config item");
    return KlsInvalidConfigItem;
    
  case KlcVerbose:
    NULL_CHECK(vP);
    return klVerboseLevelSet(compP, (char*) vP);

  case KlcReads:
    b = *((KBool*) vP);
    if ((b != KTRUE) && (b != KFALSE))
    {
      KL_ERROR_HOOK(KlsInvalidParameter, "bad value for a boolean (must be 1 or 0)");
      return KlsInvalidParameter;
    }

    if (compP == NULL)
    {
      int cIx;

      for (cIx = 0; cIx < KL_COMPONENTS_MAX; cIx++)
      {
        if (klCompV[cIx].inUse != KTRUE)
          continue;
        klCompV[cIx].reads = KTRUE;
      }
    }
    else
      compP->reads = b;
    // KBL_M(("reads: %s", (compP->reads == KTRUE)? "TRUE" : "FALSE"));    
    return KlsOk;

    
  case KlcWrites:
    b = *((KBool*) vP);
    if ((b != KTRUE) && (b != KFALSE))
    {
      KL_ERROR_HOOK(KlsInvalidParameter, "bad value for a boolean (must be 1 or 0)");
      return KlsInvalidParameter;
    }

    if (compP == NULL)
    {
      int cIx;

      for (cIx = 0; cIx < KL_COMPONENTS_MAX; cIx++)
      {
        if (klCompV[cIx].inUse != KTRUE)
          continue;
        klCompV[cIx].writes = KTRUE;
      }
    }
    else
      compP->writes = KTRUE;
    return KlsOk;
    
  case KlcFixme:
    b = *((KBool*) vP);
    if ((b != KTRUE) && (b != KFALSE))
    {
      KL_ERROR_HOOK(KlsInvalidParameter, "bad value for a boolean (must be 1 or 0)");
      return KlsInvalidParameter;
    }

    if (compP == NULL)
    {
      int cIx;

      for (cIx = 0; cIx < KL_COMPONENTS_MAX; cIx++)
      {
        if (klCompV[cIx].inUse != KTRUE)
          continue;
        klCompV[cIx].fixme = KTRUE;
      }
    }
    else
      compP->fixme = KTRUE;
    return KlsOk;
    
  case KlcTraceLevelSet:
    // No NULL_CHECK here - klTraceLevelSet accepts NULL traceLevel
    ks = klTraceLevelSet(compP, (char*) vP);
    return ks;

  case KlcLogFileLineFormat:
    NULL_CHECK(vP);
    if (klLogFileFormat != NULL)
      free(klLogFileFormat);
    klLogFileFormat = strdup((char*) vP);
    return KlsOk;
    
  case KlcScreenLineFormat:
    NULL_CHECK(vP);
    if (klScreenFormat != NULL)
      free(klScreenFormat);
    klScreenFormat = strdup((char*) vP);
    return KlsOk;

  case KlcLogToScreen:
    // No NULL_CHECK here - vP is not used for this request
    klScreenFd = 1;
    return KlsOk;
    
  case KlcLogToStderr:
    // No NULL_CHECK here - vP is not used for this request
    klScreenFd = 2;
    return KlsOk;

  case KlcErrorHook:
    // No NULL_CHECK here, it's OK to be NULL
    klErrorHookF = (KlErrorHook) vP;
    return KlsOk;

  case KlcErrorHookParam:
    // No NULL_CHECK here, it's OK to be NULL
    klErrorHookP = vP;
    return KlsOk;

  case KlcLogHook:
    // No NULL_CHECK here, it's OK to be NULL
    klLogHookF = (KlLogHook) vP;
    return KlsOk;

  case KlcLogHookParam:
    // No NULL_CHECK here, it's OK to be NULL
    klLogHookP = vP;
    return KlsOk;
  }

  KL_ERROR_HOOK(KlsInvalidConfigItem, "invalid config item II");
  return KlsInvalidConfigItem;
}
