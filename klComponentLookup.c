// 
// FILE            klComponentLookup.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "klog/KlComponent.h"                // KlComponent, klCompV, klCompNo
#include "klog/klTraceLevelV.h"              // KL_TRACE_LEVELS
#include "klog/klInit.h"                     // KL_INITALIZED_CHECK
#include "klog/klComponentLookup.h"          // Own interface



// -----------------------------------------------------------------------------
//
// globalComponent - special component containing ALL of the trace levels
//
static KlComponent globalComponent =
{
  KTRUE,
  "Global",
  KL_TRACE_LEVELS - 1,
  0,
  NULL,
  KFALSE,
  KFALSE,
  KFALSE
};



// -----------------------------------------------------------------------------
//
// klGlobalComponentGet -
//
KlComponent* klGlobalComponentGet(void)
{
  return &globalComponent;
}



// -----------------------------------------------------------------------------
//
// klComponentLookup -
//
KlComponent* klComponentLookup(char* name)
{
  int ix;

  KL_INITALIZED_CHECK(NULL);

  if (strcmp(name, "Global") == 0)
    return &globalComponent;

  for (ix = 0; ix < klCompNo; ix++)
  {
    if (klCompV[ix].inUse == KFALSE)
      continue;

    if (strcmp(klCompV[ix].name, name) == 0)
      return &klCompV[ix];
  }

  return NULL;
}
