// 
// FILE            KlStatus.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "klog/KlStatus.h"                   // Own interface



// -----------------------------------------------------------------------------
//
// klStatus -
//
char* klStatus(KlStatus ks)
{
  switch (ks)
  {
  case KlsOk:                              return "Ok";

  case KlsNotInitialized:                  return "Not Initialized";
  case KlsAlreadyInitialized:              return "Already Initialized";
  case KlsInvalidParameter:                return "Invalid Parameter";
  case KlsBufferTooSmall:                  return "Buffer Not Big Enough";

  case KlsMalloc:                          return "alloc error";
  case KlsSemInit:                         return "sem_init error";
  case KlsSemTake:                         return "sem_wait error";
  case KlsSemGive:                         return "sem_post error";
  case KlsWriteToLogFile:                  return "Write to Log File";
  case KlsWriteToScreen:                   return "Write to Screen";

  case KlsLogFileExistsButIsNotWritable:   return "Log File Exists But Is Not Writable";
  case KlsLogFileNotWritable:              return "Log File Not Writable";
  case KlsNoWritePermissionsOnDirectory:   return "No Write Permissions On Directory";
  case KlsNoSuchDirectory:                 return "No Such Directory";
  case KlsPartialWriteToLogFile:           return "Partial Write to Log File";
  case KlsPartialWriteToScreen:            return "Partial Write to Screen";
  case KlsInvalidTraceLevel:               return "Invalid Trace Level";
  case KlsErrorOpeningLogFile:             return "Error Opening Log File";
  case KlsLogFilePathTooLong:              return "Log File Path Too Long";
  case KlsInvalidConfigItem:               return "Invalid Config Item";
  case KlsInvalidCharacter:                return "Invalid Character";
  case KlsNoSpaceForComponent:             return "No Space For Component";
  case KlsTooManyTraceLevelsForComponent:  return "Too Many Trace Levels For Component";
  case KlsOutOfTraceLevels:                return "Out of Trace Levels";
  case KlsInvalidOffset:                   return "Invalid Offset";
  case KlsNoComponent:                     return "No Component";
  case KlsInvalidOffsetForComponent:       return "Invalid Offset For Component";
  case KlsComponentNotFound:               return "Component Not Found";
  case KlsComponentAlreadyExists:          return "Component Already Exists";
  case KlsNoComponentName:                 return "No Component Name";
  case KlsMultipleComponents:              return "Multiple Components Where Not Permitted (Global Is Used)";
  }

  return "Unknown KLog Status Code";
}
