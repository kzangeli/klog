// 
// FILE            klTraceLevelSet.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdlib.h>                          // atoi, free

#include "kbase/kMacros.h"                   // K_FT

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klComponentLookup.h"          // klComponentLookup, klGlobalComponentGet
#include "klog/klTraceLevelV.h"              // klTraceLevelV
#include "klog/klHooks.h"                    // KL_ERROR_HOOK, ...
#include "klog/klTraceLevelSet.h"            // Own interface



// -----------------------------------------------------------------------------
//
// traceLevelSet -
//
// tItem is either a number (e.g. "14"), or a range ("e.g. 0-24"), or a special keyword All/None.
// There are also two special ranges:
//   X-    (which means: trace levels X to max-trace-level-for-component)
//   -X    (which means: 0-X)
// Both special ranges include X as a trace level to set/reset.
//
// This function sets the corresponding indices of 'tItem' to KTRUE/KFALSE in klTraceLevelV.
//
static KlStatus traceLevelSet(KlComponent* compP, char* tItem, KBool value)
{
  if (compP == NULL)
  {
    KL_ERROR_HOOK(KlsNoComponent, "component is null");
    return KlsNoComponent;
  }

  // Paranoid Range check
  if (compP->offset >= sizeof(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsInvalidOffset, "component offset is off the charts");
    return KlsInvalidOffset;
  }

  // Paranoid Range check II
  if (compP->offset + compP->tLevs >= sizeof(klTraceLevelV))
  {
    KBL_E(("Paranoid Range check II. %d+%d, >= %lu", compP->offset, compP->tLevs, sizeof(klTraceLevelV)));
    KL_ERROR_HOOK(KlsInvalidOffset, "component offset + max trace level is off the charts");
    return KlsInvalidOffset;
  }

  //
  // Special tItems: All and None
  //
  if ((strcmp(tItem, "All") == 0) || (strcmp(tItem, "None") == 0))
  {
    KBL_V(("Got a '%s' for component %s", tItem, compP->name));
    
    KBool  val = (tItem[0] == 'A')? KTRUE : KFALSE;  // 'A': All, else None
    int    ix;

    //
    // NOTE: the third parameter for this function "KBool value" may invert the value of 'val'.
    //
    //       4 cases:
    //         traceLevelSet(compP, "All",  KTRUE)  must mean: Set all trace levels of compP to KTRUE
    //         traceLevelSet(compP, "None", KTRUE)  must mean: Set all trace levels of compP to KFALSE
    //         traceLevelSet(compP, "All",  KFALSE) must mean: Set all trace levels of compP to KFALSE
    //         traceLevelSet(compP, "None", KFALSE) must mean: Set all trace levels of compP to KTRUE
    //       
    if (value == KFALSE)
      val = (val == KFALSE)? KTRUE : KFALSE;


    //
    //
    // Set/Remove ALL traceLevels for component
    //
    // This is safe to do as compP->offset+compP->tLevs has been tested to be inside klTraceLevelV
    //
    KBL_V(("Setting trace levels %d-%d for component '%s'", 0, compP->tLevs - 1, compP->name));
    for (ix = 0; ix < compP->tLevs; ix++)
      klTraceLevelV[compP->offset + ix] = val;

    KBL_V(("OK"));
    return KlsOk;
  }


  //
  // Find out whether it is a range or not and detect forbidden characters
  //
  char*  cP     = tItem;
  char*  fromP  = tItem;
  char*  toP    = NULL;

  KBL_V(("cP: %s", cP));
  while (*cP != 0)
  {
    if (*cP == '-')
    {
      KBL_V(("Found hyphen: %s", cP));
      if (toP != NULL)
      {
        KBL_V(("second hyphen"));
        KL_ERROR_HOOK(KlsInvalidCharacter, "second hyphen found in trace level range");
        return KlsInvalidCharacter;
      }

      *cP = 0;
      toP = &cP[1];
    }
    else if ((*cP < '0') || (*cP > '9'))
    {
      KBL_V(("Found invalid char '%c' in trace level parameter", *cP));
      KL_ERROR_HOOK(KlsInvalidCharacter, "Invalid character in trace level parameter");
      return KlsInvalidCharacter;
    }

    ++cP;
  }

  KBL_V(("No forbidden chars"));
  //
  // A trace level range of "-22" is treated like "0-22"
  //
  if (*fromP == '-')
  {
    fromP = "0";
  }


  //
  // A trace level range of "12-" is treated like "12-<Max trace level of component>" 
  //
  char toV[64];
  if ((toP != NULL) && (*toP == 0))
  {
    snprintf(toV, sizeof(toV), "%d", compP->tLevs - 1);
    toP = toV;
  }


  if (toP == NULL)  // Single value
  {
    int singleValue = atoi(fromP);

    //
    // 01. Invalid integer (if atoi returns 0, the only string accepted is "0" - anything else is an error)
    //     However, as everything is already tests, the only way to get 0 from atoi wouls be a string of only zeroes,
    //     like "00" or "0000" ... 
    //     No need to flag an error for that. Let's just accept the value as zero.
    //
    //     So, invalid integer here is ... Impossible, the error would have been caught in an earlier stage.
    //
    //
    // 02. Negative value?
    //     Also, IMPOSSIBLE. The hyphen would have been caught as the delimiter for a range and toP
    //     would NOT BE NULL
    //
    //
    // 03. Absolute Range Check
    //     - yes, check needed in this case. The number can be very big. No check made until this one
    //
    if (singleValue >= K_VEC_SIZE(klTraceLevelV))
    {
      KL_ERROR_HOOK(KlsInvalidTraceLevel, "single value for trace level is off the charts");
      return KlsInvalidTraceLevel;
    }


    //
    // 04. Absolute Component Range Check (starting from component offset)
    //
    int vectorIx = compP->offset + singleValue;
    if (vectorIx >= sizeof(klTraceLevelV))
    {
      KL_ERROR_HOOK(KlsInvalidOffset, "absolute trace level index for single value for trace level is off the charts");
      return KlsInvalidOffset;
    }


    //
    // 05. Component Range Check
    //
    if (singleValue >= compP->tLevs)
    {
      KL_ERROR_HOOK(KlsInvalidOffsetForComponent, "single value for trace level exceeds the number of trace levels registered for component");
      return KlsInvalidOffsetForComponent;
    }

    klTraceLevelV[vectorIx] = value;
    return KlsOk;
  }


  //
  // Range
  //
  int  from = atoi(fromP);
  int  to   = atoi(toP);


  //
  // 01. Invalid integer (can only happen using "00", as 'forbidden chars' is used first to filter out this error
  //     As explained earlier, under 'singleValue', fromP cannot contain invalid chars, only 0[0]+ gives 0 from atoi
  //
  //     toP ... is already checked for invalid chars (the entire string is - where '-' is looked for), so,
  //     just like fromP, 0[0]+ is treated as a valid zero and no check is needed
  //

  //
  // 02. Negative values?
  //     Can't happen. 
  //


  //
  // 03. Index crossing (lower limit is bigger than the upper limit)
  //
  if (from > to)
  {
    KL_ERROR_HOOK(KlsInvalidTraceLevel, "lower limit for trace level range exceeds the upper limit");
    return KlsInvalidTraceLevel;
  }


  //
  // 04. Absolute Range Check
  //
  if (from >= K_VEC_SIZE(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsInvalidTraceLevel, "lower limit for trace level range is off the charts");
    return KlsInvalidTraceLevel;
  }

  if (to >= K_VEC_SIZE(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsInvalidTraceLevel, "upper limit for trace level range is off the charts");
    return KlsInvalidTraceLevel;
  }


  //
  // 05. Absolute Component Range Check (starting from component offset)
  //
  if (compP->offset + from >= sizeof(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsInvalidOffset, "absolute trace level index of lower limit for trace level range is off the charts");
    return KlsInvalidOffset;
  }

  if (compP->offset + to >= sizeof(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsInvalidOffset, "absolute trace level index of upper limit for trace level range is off the charts");
    return KlsInvalidOffset;
  }
  

  //
  // 06. Component Range Check
  //
  if (from >= compP->tLevs)
  {
    KL_ERROR_HOOK(KlsInvalidOffsetForComponent, "lower limit for trace level range exceeds the number of trace levels registered for component");
    return KlsInvalidOffsetForComponent;
  }

  if (to >= compP->tLevs)
  {
    KL_ERROR_HOOK(KlsInvalidOffsetForComponent, "upper limit for trace level range exceeds the number of trace levels registered for component");
    to = compP->tLevs - 1;
  }
      

  //
  // 07. All good, let's set the trace levels asked for, including 'from' and 'to'
  //
  KBL_V(("Setting trace levels %d-%d for component '%s'", from, to, compP->name));
  int ix;
  for (ix = from; ix <= to; ix++)
    klTraceLevelV[compP->offset + ix] = value;

  return KlsOk;
}  



// -----------------------------------------------------------------------------
//
// traceLevelListSet - parse comma-separated trace level string and call traceLevelSet
//
static KlStatus traceLevelListSet(KlComponent* compP, char* tLevs)
{
  if (compP == NULL)
  {
    KL_ERROR_HOOK(KlsNoComponent, "component is null");
    return KlsNoComponent;
  }

  char* toFree = strdup(tLevs);

  if (toFree == NULL)
  {
    KL_ERROR_HOOK(KlsMalloc, "error allocating memory for 'traceLevels'");
    KBL_RP(KlsMalloc, ("strdup"));
  }

  char*     start  = toFree;
  char*     tP     = start;
  int       itemNo = 0;
  KlStatus  ks;

  KBL_V(("comp:%s, tLevs: '%s'", compP->name, tLevs));
  while (*tP != 0)
  {
    // Find comma, end string and execute traceLevelSet on the item
    if (*tP == ',')
    {
      *tP = 0;

      KBL_V(("Calling traceLevelSet (item %d) for '%s'", itemNo, start));
      if ((ks = traceLevelSet(compP, start, KTRUE)) != KlsOk)
      {
        // KL_ERROR_HOOK already invoked by traceLevelSet
        free(toFree);
        return ks;
      }
      
      start   = &tP[1];
      itemNo += 1;
    }

    ++tP;
  }

  // And the last item
  KBL_V(("Calling traceLevelSet (last item %d) for '%s'", itemNo, start));
  if ((ks = traceLevelSet(compP, start, KTRUE)) != KlsOk)
  {
    // KL_ERROR_HOOK already invoked by traceLevelSet
    free(toFree);
    return ks;
  }

  free(toFree);
  return KlsOk;
}  



// -----------------------------------------------------------------------------
//
// klTraceLevelSet - add to the current set of active trace levels
//
//   This function can be called in a few different ways:
//
//     1. Single component:
//       klTraceLevelSet(compP, "0-5,11")
//
//     2. List of Components:
//       klTraceLevelSet(NULL, "comp1:0-5,11;comp2:0-255;comp3:11")
//
//     3. Special grouping words:
//       klTraceLevelSet(compP, "All")
//       klTraceLevelSet(compP, "None")
//       klTraceLevelSet(NULL, "comp1:All;comp2:None")
//       klTraceLevelSet(NULL, "comp1:None")
//
//     4. Global:
//       klTraceLevelSet(NULL, "Global:All")
//       klTraceLevelSet(NULL, "Global:None")
//       klTraceLevelSet(NULL, "Global:0-4,101-105,202-400")
//
//   Special values: "None" and "All", to turn off/on all trace levels for a component
//
//   The format of the 'trace level string' is: "t1-t2,t3,t4-t5,t6". E.g.: "0-4,9,21-23,29"
//
//   I.e. a comma separated list of numbers and ranges.
//   The string can *only* contain the chars hyphen, comma, and digits: -,0-9
//
// RETURN VALUES
//   On successful operation, KlsOk is returned.
//   On error, the following errors codes are returned 'directly':
//     - KlsInvalidParameter: second parameter 'traceLevels' is NULL
//     - KlsMultipleComponents: Semi-colon found in a trace level string that uses 'Global' - that is forbidden
//     - KlsNoComponentName: missing component name in trace level string
//     - KlsComponentNotFound: the component X in the trace level string "X:0-5" is not found
//
//   However, underlying function traceLevelListSet(), which in its turn depends on traceLevelSet(),
//   may indirectly make klTraceLevelSet return the following error codes: 
//     - KlsMalloc: out of memory 
//     - KlsNoComponent (cannot happen, at least in theory ... ;-))
//     - KlsInvalidOffset - component offset is off the charts (offset points beyond all possible trace levels)
//     - KlsInvalidOffset - component offset+tLevs is off the charts ("offset + max trace level" points beyond all possible trace levels)
//     - 
//
//    [ Only the first two come from traceLevelListSet, the rest come from traceLevelSet ]
//
KlStatus klTraceLevelSet(KlComponent* compP, char* traceLevels)
{
  KBL_V(("klTraceLevelSet: component %p, traceLevels '%s'", compP, traceLevels));
  //
  // traceLevels == NULL?
  //
  if (traceLevels == NULL)
  {
    KL_ERROR_HOOK(KlsInvalidParameter, "traceLevels parameter is NULL");
    return KlsInvalidParameter;
  }


  if (compP == NULL)
  {
    //
    // Four possibilities here:
    // - List of Components:   "comp1:0-5,11;comp2:0-255;comp3:All"
    // - Global:               "Global:All"
    //                         "Global:None"
    //                         "Global:0-4,101-105,202-400"
    //
    // To solve the "Global problem" in an easy way, a special component, named "Global"
    // has been invented.
    //
    // 'Global' can not be used in "List of Components".
    // Could be admitted, but I think it's a good idea not to mix. Better to return error.
    //
    // FIXME: Check that no unused trace levels are set via 'Global:x-y' (not that it matters)
    // FIXME: Zero out all component trace levels when creating the component
    //
    if (strncmp(traceLevels, "Global:", 7) == 0)
    {
      char*         levels  = &traceLevels[7];
      KlComponent*  globalP = klGlobalComponentGet();

      //
      // if "Global" is used, it can only be used once, and no other component can be used.
      // To check for occurrence of ';' is enough, traceLevelListSet/traceLevelSet takes care of
      // the rest of the validity checks
      //
      if (strchr(traceLevels, ';') != NULL)
      {
        KL_ERROR_HOOK(KlsMultipleComponents, "Semi-colon found in a trace level string that uses 'Global' - that is forbidden");
        return KlsMultipleComponents;
      }

      // Validity check done in traceLevelListSet
      return traceLevelListSet(globalP, levels);
    }
    else  // List of Components?
    {
      //
      // Here we need to modify the incoming string. Must make a copy
      //
      char* tLevs  = strdup(traceLevels);
      char* toFree = tLevs;

      if (tLevs == NULL)
      {
        KL_ERROR_HOOK(KlsMalloc, "error allocating memory for 'traceLevels'");
        KBL_RP(KlsMalloc, ("strdup"));
      }

      KBL_V(("List of Components?"));
      KBL_V(("tLevs: '%s'", tLevs));
      while (*tLevs != 0)
      {
        char* semiColon = NULL;

        // 1. Find ';', if any, and null it out if found
        if ((semiColon = strchr(tLevs, ';')) != NULL)
        {
          *semiColon = 0;
          KBL_V(("Found semiColon. Action is: '%s'", tLevs));
        }
        else
          KBL_V(("No semiColon. Last action is: '%s'", tLevs));
        
        // 2. Find name of component
        char* compName = tLevs;
        char* colon    = strchr(tLevs, ':');

        if (colon == NULL)
        {
          KBL_V(("KlsNoComponentName - calling ERROR_HOOK"));
          KL_ERROR_HOOK(KlsNoComponentName, "no colon found in trace-level string => no component name found");
          free(toFree);
          return KlsNoComponentName;
        }

        *colon = 0;
        tLevs  = &colon[1];
        KBL_V(("component: '%s', tLevs: '%s'", compName, tLevs));

        // 3. Lookup component
        compP = klComponentLookup(compName);
        if (compP == NULL)
        {
          KL_ERROR_HOOK(KlsComponentNotFound, "component in trace level string was not found");
          free(toFree);
          return KlsComponentNotFound;
        }
        KBL_V(("Found component '%s'", compP->name));
        
        // 4. Call traceLevelListSet for component
        KlStatus ks;

        KBL_V(("Calling traceLevelListSet('%s', '%s')", compP->name, tLevs));
        if ((ks = traceLevelListSet(compP, tLevs)) != KlsOk)
        {
          // Error Hook already called by traceLevelListSet or traceLevelSet
          free(toFree);
          return ks;
        }

        // 5. Move 'tLevs' to point to next semi-colon-separated item
        if (semiColon == NULL)
          break;
        
        tLevs = &semiColon[1];
        KBL_V(("Loop continues. Rest: '%s'", tLevs));
      }
      free(toFree);
    }

    return KlsOk;
  }


  // 
  // So, component is given ...
  //
  // The possibilities here are:
  //
  //     1. Single component:
  //       klTraceLevelSet(compP, "0-5,11")
  //
  //     3. Special grouping words:
  //       klTraceLevelSet(compP, "All")
  //       klTraceLevelSet(compP, "None")
  //



  //
  // traceLevelListSet does the work of parsing the 'traceLevels' string for comma-separated items
  // and calls traceLevelSet() for each item.
  //
  // Any error hooks are also taken care of in there
  //
  KBL_V(("calling traceLevelListSet with traceLevels '%s'", traceLevels));
  return traceLevelListSet(compP, traceLevels);
}  



// -----------------------------------------------------------------------------
//
// klTraceLevelIsSet
//

// -----------------------------------------------------------------------------
//
// klTraceLevelGet - TBI
//
