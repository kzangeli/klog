// 
// FILE            klBufferPresent.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLBUFFERPRESENT_H_
#define KLOG_KLBUFFERPRESENT_H_

#include "klog/KlComponent.h"                // KlComponent



// -----------------------------------------------------------------------------
//
// klBufferPresent -
//
extern void klBufferPresent
(
  KlComponent*  compP,
  char          type,
  const char*   fileName,
  int           lineNo,
  const char*   functionName,
  char*         buf,
  int           bufSize
);

#endif  // KLOG_KLBUFFERPRESENT_H_
