// 
// FILE            klog.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "klog/KlStatus.h"              // KlStatus
#include "klog/klInit.h"                // klInit
#include "klog/kLog.h"                  // K Log Macros
#include "klog/KlComponent.h"           // KlComponent
#include "klog/klComponentDestroy.h"    // klComponentDestroy
#include "klog/klComponentLookup.h"     // klComponentLookup
#include "klog/klComponentPresent.h"    // klComponentPresent
#include "klog/klComponentRegister.h"   // klComponentRegister
#include "klog/klComponentsPresent.h"   // klComponentsPresent
#include "klog/klConfig.h"              // klConfig
#include "klog/klConfigVars.h"          // klConfigVars
#include "klog/klDestroy.h"             // klDestroy
#include "klog/klFreeText.h"            // klFreeText
#include "klog/klHooks.h"               // K Log Hooks
#include "klog/klMsgPrint.h"            // klMsgPrint
#include "klog/klSem.h"                 // K Log Semaphore
#include "klog/klTraceLevelGet.h"       // klTraceLevelGet
#include "klog/klTraceLevelIsSet.h"     // klTraceLevelIsSet
#include "klog/klTraceLevelSet.h"       // klTraceLevelSet
#include "klog/klTraceLevelV.h"         // klTraceLevelV
#include "klog/klVerboseLevelSet.h"     // klVerboseLevelSet
#include "klog/version.h"               // KLOG_VERSION
#include "klog/klTraceLevelPresent.h"   // klTraceLevelPresent
