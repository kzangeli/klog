// 
// FILE            klFreeText.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLFREETEXT_H_
#define KLOG_KLFREETEXT_H_



// -----------------------------------------------------------------------------
//
// KL_FREETEXT_MAX_SIZE - maximum length of the free-text part a log line
//
#define KL_FREETEXT_MAX_SIZE  1024



// -----------------------------------------------------------------------------
//
// klFreeText - all the free-text into freeTextBuffer
//
// NOTE
//   It is OK to use a single buffer for this as klFreeText is only called with
//   the klog semaphore taken.
//
extern char* klFreeText(const char* format, ...);

#endif  // KLOG_KLFREETEXT_H_
