// 
// FILE            klInit.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <fcntl.h>                           // open()
#include <time.h>                            // time_t, time(), etc
#include <unistd.h>                          // write()

#include "kbase/kTypes.h"                    // KBool
#include "kbase/kBasicLog.h"                 // Basic log macros using printf

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klHooks.h"                    // hook functionality
#include "klog/klSem.h"                      // klSemInit()
#include "klog/klInit.h"                     // Own interface



// -----------------------------------------------------------------------------
//
// progName - 
//
static char* progName = "no prog name";



// -----------------------------------------------------------------------------
//
// klogInitialized - is the library already initialized?
//
KBool klogInitialized = KFALSE;



// -----------------------------------------------------------------------------
//
// logFilePath - path of the log file
//
static char logFilePath[256];



// -----------------------------------------------------------------------------
//
// klLogFd - file descriptor of log file
//
int klLogFd    = -1;



// -----------------------------------------------------------------------------
//
// klInit -
//
// PARAMETERS
//   _progName:     the name of the executable
//   logDir:        the directory where to create the log file (default is /tmp)
//   toFile:        if KFALSE, to log file is created, stdout is used instead
//   _logFileName:  if non-NULL, used as name of the log file (if NULL, _progName is used)
//
KlStatus klInit(char* _progName, char* logDir, KBool toFile, char* _logFileName)
{
  char* logFileName = (_logFileName == NULL)? _progName : _logFileName;
  progName = _progName;

  if (klogInitialized == KTRUE)
  {
    KL_ERROR_HOOK(KlsAlreadyInitialized, "klog library is already initialized");
    return KlsAlreadyInitialized;
  }

  
  //
  // Default log directory is "/tmp"
  //
  if (logDir == NULL)
    logDir = "/tmp";


  //
  // Check that the path of the log file fits inside 'logFilePath'
  //
  int logFileNameLen  = strlen(logFileName);
  int logDirLen       = strlen(logDir);


  //
  // logDir "/" logFileName ".log" => 6 more chars (including zero termination) needed
  //
  if (logDirLen + logFileNameLen + 6 > sizeof(logFilePath))  // 6: strlen(".log") + slash + zero termination
  {
    KL_ERROR_HOOK(KlsLogFilePathTooLong, "total length of log file path is too long");
    return KlsLogFilePathTooLong;
  }
  snprintf(logFilePath, sizeof(logFilePath), "%s/%s.log", logDir, logFileName);

  //
  // Make sure logDir is really a directory
  // - and that it is writeable
  //
  if (access(logDir, F_OK) == -1)
  {
    KL_ERROR_HOOK(KlsNoSuchDirectory, "the directory given for the log file does not exist");
    return KlsNoSuchDirectory;
  }
  else if (access(logDir, W_OK) == -1)
  {
    KL_ERROR_HOOK(KlsNoWritePermissionsOnDirectory, "the directory given for the log file has insufficient permissions");
    return KlsNoWritePermissionsOnDirectory;
  }


  //
  // Log file exists but is not writable?
  //
  if ((access(logFilePath, F_OK) == 0) && (access(logFilePath, W_OK) == -1))
  {
    KL_ERROR_HOOK(KlsLogFileExistsButIsNotWritable, "the log file exists and has insufficient permissions - can't be used");
    return KlsLogFileExistsButIsNotWritable;
  }
  

  //
  klSemInit();
  if (toFile == KFALSE)
  {
    klogInitialized = KTRUE;
    return KlsOk;
  }


  // FIXME: Save old logfile to .old or with date or ...
  klLogFd = open(logFilePath, O_WRONLY | O_CREAT | O_TRUNC, 0664);
  // If open fails ... no logFile ... klLogFd == -1 takes care of it

  if (klLogFd == -1)
  {
    KL_ERROR_HOOK(KlsErrorOpeningLogFile, "error opening the log file");
    return KlsErrorOpeningLogFile;
  }

  //
  // Write an initial message in the log file
  //
  time_t     now = time(NULL);
  struct tm  tm;
  char       tmBuf[80];
  char       msg[256];
  int        nb;

  gmtime_r(&now, &tm);
  strftime(tmBuf, sizeof(tmBuf), "%A %d %h %H:%M:%S %Y", &tm);
  snprintf(msg, sizeof(msg), "%s started %s\n================================================================================\n",
           progName, tmBuf);

  nb = write(klLogFd, msg, strlen(msg));

  if (nb == -1)
  {
    KL_ERROR_HOOK(KlsWriteToLogFile, "error writing header to log file");
    KBL_P(("initial write to log file"));
    return KlsWriteToLogFile;
  }
  else if (nb != strlen(msg))
  {
    KL_ERROR_HOOK(KlsWriteToLogFile, "written just a part of the header to the log file");
    KBL_E(("initial write to log file - written %d bytes, not %ld as expected", nb, strlen(msg)));
  }

  klogInitialized = KTRUE;
  return KlsOk;
}



// -----------------------------------------------------------------------------
//
// klExit - cleanup
//
void klExit(void)
{
  progName         = "no prog name";
  klogInitialized  = KFALSE;

  bzero(logFilePath, sizeof(logFilePath));
}
