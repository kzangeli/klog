# klog - k suite log library

*Work In Progress* - this library is still FAR from ready.

## Table of Contents
FIXME

## Background
The klog library is used throughout the suite of 'k libs', unifying the
logging into one single logfile for the libraries and it is strongly recommended
for an application based on this suite of libraries to use klog as well.

For each library to have its own verbose mode and trace levels etc, the klog library 
uses *klog components*, as units to distiguish between the different libraries
that all use the same logging facility. A log component gets a name and the
verbosity of each component can be modified using the name of the component ...

## Install
% make di

## Usage
I have big plans for this library, just haven't had much time for it yet.
My old log library was incorporated into the [Orion context Broker](https://github.com/telefonicaid/fiware-orion) and
I lost ownership of it.
Now, with many lessons learned I will develop a much better log library, as soon as I have time.
Very little of this library is in use anywhere and it should just be ignored until I get time to do something about it

The K-libs right now use a mechanism I implemented for [kjson](https://gitlab.com/kzangeli/kjson), in the module `kjLog.[ch]`,
and this "mechanism" will be moved to the `klog` library in the near future.

## License
[Apache 2.0](LICENSE) © 2020 Ken Zangelin
