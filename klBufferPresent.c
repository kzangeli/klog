// 
// FILE            klBufferPresent.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdlib.h>                          // malloc
#include <ctype.h>                           // isprint

#include "klog/KlStatus.h"                   // KlsWriteToLogFile, KlsWriteToScreen, etc.
#include "klog/klMsgPrint.h"                 // klMsgPrint
#include "klog/klBufferPresent.h"            // Own interface


static char printBuf[1024 * 64];
// -----------------------------------------------------------------------------
//
// klBufferPresent -
//
void klBufferPresent
(
  KlComponent*  compP,
  char          type,
  const char*   fileName,
  int           lineNo,
  const char*   functionName,
  char*         buf,
  int           bufSize
)
{
  int ix = 0;

  bzero(printBuf, sizeof(printBuf));

  if (type == 'W')
    sprintf(printBuf, "Written %d bytes:\n", bufSize);
  else if (type == 'R')
    sprintf(printBuf, "Read %d bytes:\n", bufSize);
  else
    sprintf(printBuf, "Buffer of %d bytes:\n", bufSize);

  while (ix < bufSize)
  {
    char temp[16];

    if (ix % 16 == 0)
    {
      sprintf(temp, "%04x: ", ix);
      strcat(printBuf, temp);
    }
    
    sprintf(temp, "%02x ", buf[ix] & 0xFF);
    strcat(printBuf, temp);

    ++ix;

    if (ix % 16 == 0)
    {
      int jx = ix - 16;

      strcat(printBuf, "  ");
      while (jx < ix)
      {
        char c[2];
        
        c[0] = (isprint(buf[jx]))? buf[jx] : ' ';
        c[1] = 0;

        strcat(printBuf, c);
        ++jx;
      }
      
      strcat(printBuf, "\n");
    }
  }

  strcat(printBuf, "\n");
  
  klMsgPrint(compP, type, fileName, lineNo, functionName, printBuf, 0);
}
