// 
// FILE            klComponentRegister.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stddef.h>                          // NULL
#include <stdlib.h>                          // free

#include "klog/klInit.h"                     // KL_INITALIZED_CHECK
#include "klog/klHooks.h"                    // hook functionality
#include "klog/klTraceLevelV.h"              // klTraceLevelOffset
#include "klog/KlComponent.h"                // KlComponent, KlComponentTraceLevelInfo, etc
#include "klog/klComponentLookup.h"          // klComponentLookup
#include "klog/klComponentRegister.h"        // Own interface



// -----------------------------------------------------------------------------
//
// klComponentRegister - register a klog component
//
// NOTE
//   tLevInfoP is used AS IS, just copying the pointer
//   This means that tLevInfoP must point to memory that is not to be modified
//   or freed or anything like that.
//
//   Also, if a component is registering with a value of 5 for 'noOfTraceLevels',
//   then tLevInfoP had better contain 5 items.
//   If not, we will have a serious problem ... SIGSEGV it is called
//
KlComponent* klComponentRegister(char* name, int noOfTraceLevels, KlComponentTraceLevelInfo* tLevInfoP)
{
  KL_INITALIZED_CHECK(NULL);

  if ((name == NULL) || (*name == 0))
  {
    KL_ERROR_HOOK(KlsInvalidParameter, "component name is NULL or EMPTY");
    return NULL;
  }

  if (klCompNo >= KL_COMPONENTS_MAX)
  {
    KL_ERROR_HOOK(KlsNoSpaceForComponent, "All log components are already used");
    return NULL;
  }

  if (noOfTraceLevels > KL_MAX_TRACE_LEVELS_PER_COMPONENT)
  {
    KL_ERROR_HOOK(KlsTooManyTraceLevelsForComponent, "number of trace levels for component exceeds the maximum allowed");
    return NULL;
  }
  
  if ((klTraceLevelOffset + noOfTraceLevels) > sizeof(klTraceLevelV))
  {
    KL_ERROR_HOOK(KlsOutOfTraceLevels, "out of trace levels (this can't happen - not unless provoked)");
    return NULL;
  }

  if (klComponentLookup(name) != NULL)
  {
    KL_ERROR_HOOK(KlsComponentAlreadyExists, "a component with that name already exixts");
    return NULL;
  }

  KlComponent* compP = &klCompV[klCompNo];
  ++klCompNo;

  // Missing cleanup?
  if (compP->name != NULL)
    free(compP->name);

  // Now zero out the entire component
  bzero(compP, sizeof(KlComponent));

  // And set the new values
  compP->name         = strdup(name);
  compP->offset       = klTraceLevelOffset;
  compP->tLevs        = noOfTraceLevels;
  compP->tLevInfoV    = tLevInfoP;
  compP->inUse        = KTRUE;

  // FIXME: Check compP->tLevInfoV?

  klTraceLevelOffset += noOfTraceLevels;

  KBL_V(("Registered KL component '%s'", compP->name));
  return compP;
}
