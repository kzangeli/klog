// 
// FILE            klTraceLevelGet.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <strings.h>                         // bzero

#include "klog/KlStatus.h"                   // KlStatus
#include "klog/klInit.h"                     // KL_INITALIZED_CHECK
#include "klog/klHooks.h"                    // KL_ERROR_HOOK
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klTraceLevelV.h"              // klTraceLevelV
#include "klog/klTraceLevelGet.h"            // Own interface



// -----------------------------------------------------------------------------
//
// klTraceLevelGet - convert trace level bool vector 'back' to string
//
KlStatus klTraceLevelGet(KlComponent* compP, char* out, int outLen)
{
  int startIx = -1;
  int endIx   = -1;
  int ix;

  KL_INITALIZED_CHECK(KFALSE);

  if (compP == NULL)
  {
    KL_ERROR_HOOK(KlsInvalidParameter, "Component is NULL");
    return KFALSE;
  }

  //
  // Clear out buffer
  //
  bzero(out, outLen);


  for (ix = 0; ix <= compP->tLevs; ix++)
  {
    if ((klTraceLevelV[compP->offset + ix] == KTRUE) && (ix != compP->tLevs))
    {
      if (startIx == -1)
        startIx = ix;
      else
        endIx = ix;
    }
    else
    {
      if (startIx != -1)
      {
        // group just ended - time to report 
        char part[32];

        if (endIx == -1)  // Single value
        {
          snprintf(part, sizeof(part), "%d", startIx);
        }
        else  // range ... or ... two single values ...
        {
          if (endIx == startIx + 1)
          {
            //
            // Decision to be made:
            //   Should e.g. 8 and 9 (NOT 7, NOT 10), be presented as
            //   - RANGE:    8-9   OR
            //   - VALUES:   8,9    ?
            //
            // Right now: RANGE [ but easy to make configurable if needed ]
            //
            snprintf(part, sizeof(part), "%d-%d", startIx, endIx);
          }
          else
          {
            snprintf(part, sizeof(part), "%d-%d", startIx, endIx);
          }

          startIx = -1;
          endIx   = -1;
        }

        //
        //
        // Still room in 'out'?
        //
        // IMPORTANT: strncat is not needed, as space problems are detected here.
        //            the +1 in 'strlen(part) + 1' is for the comma that is most
        //            probably needed if we are close to run out of space.
        //
        if (outLen <= (strlen(out) + strlen(part) + 1))
        {
          KL_ERROR_HOOK(KlsBufferTooSmall, "not space enough in out-buffer for trace level string");
          return KlsBufferTooSmall;
        }

        //
        // Comma needed if 'out' is not empty
        //
        if (out[0] != 0)
          strcat(out, ",");

        //
        // Now add to 'out'
        //
        strncat(out, part, outLen - strlen(out));

        //
        // Reset the range counters
        //
        startIx = -1;
        endIx   = -1;
      }
    }
  }

  return KlsOk;
}
