// 
// FILE            KlStatus.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLSTATUS_H_
#define KLOG_KLSTATUS_H_



// -----------------------------------------------------------------------------
//
// KlStatus -
//
typedef enum KlStatus
{
  KlsOk,

  KlsMalloc = 10,
  KlsSemInit,
  KlsSemTake,
  KlsSemGive,
  KlsWriteToLogFile,
  KlsWriteToScreen,

  KlsNotInitialized = 20,
  KlsAlreadyInitialized,
  KlsInvalidParameter,
  KlsBufferTooSmall,

  KlsNoSuchDirectory = 30,
  KlsLogFilePathTooLong,
  KlsErrorOpeningLogFile,
  KlsLogFileExistsButIsNotWritable,
  KlsLogFileNotWritable,
  KlsNoWritePermissionsOnDirectory,
  KlsPartialWriteToLogFile,
  KlsPartialWriteToScreen,

  KlsInvalidTraceLevel = 40,
  KlsTooManyTraceLevelsForComponent,
  KlsOutOfTraceLevels,
  KlsInvalidOffset,
  KlsInvalidCharacter,

  KlsNoSpaceForComponent = 50,
  KlsNoComponent,
  KlsInvalidOffsetForComponent,
  KlsComponentNotFound,
  KlsComponentAlreadyExists,
  KlsNoComponentName,
  KlsMultipleComponents,

  KlsInvalidConfigItem = 60
} KlStatus;



// -----------------------------------------------------------------------------
//
// klStatus - 
//
extern char* klStatus(KlStatus ks);

#endif  // KLOG_KLSTATUS_H_
