// 
// FILE            KlComponent.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLIB_KLCOMPONENT_H_
#define KLIB_KLCOMPONENT_H_

#include "kbase/kTypes.h"                    // KBool
#include "kbase/kMacros.h"                   // K_FT
#include "kbase/kBasicLog.h"                 // Basic log macros using printf



// -----------------------------------------------------------------------------
//
// Maximum Number of Components
//
#define KL_COMPONENTS_MAX      10



// -----------------------------------------------------------------------------
//
// KL_MAX_TRACE_LEVELS_PER_COMPONENT - 
//
#define KL_MAX_TRACE_LEVELS_PER_COMPONENT 100



// -----------------------------------------------------------------------------
//
// KlComponentTraceLevelInfo - 
//
typedef struct KlComponentTraceLevelInfo
{
  int   ix;       // Index of trace level (to add to KlComponent::offset for 'real' index)
  char* name;     // Name of trace level
} KlComponentTraceLevelInfo;



// -----------------------------------------------------------------------------
//
// KlComponent - collection of trace levels to turn on/off as a group
//
// A KlComponent is typically a library, or a distinct part of a library.
// This KL component is used to tie together a group of log messages, to turn
// verbose and traceleves on and off as a group.
//
// A CLI option for this, to set an initial state could look like this:
// $EXEC -t kjson:0-14,24;krest:5,7,9 -v kjson,krest.
//
// A KL component must be registered before it can be used:
//
// E.g.:
//   KlComponent* compP = klComponentRegister("", 20, kcompV);
//
// DOUBT:
//   Should the trace level vector reside inside KlComponent ... ?
//   Right now, all components share a BIG trace level vector and the
//   offset inside that vector is stored in KlComponent.
//
typedef struct KlComponent
{
  KBool                       inUse;        // if KTRUE, this slot is in use
  char*                       name;         // Name of component
  int                         tLevs;        // Number of trace levels for component
  int                         offset;       // Offset where trace levels start
  KlComponentTraceLevelInfo*  tLevInfoV;    // Info about each trace level
  KBool                       verbose;      // verbose for this component (KL_V)
  KBool                       fixme;        // fixme for this component   (KL_F)
  KBool                       doubt;        // doubt for this component   (KL_D)
  KBool                       reads;        // read buffer (KL_READ)
  KBool                       writes;       // written buffer (KL_WRITE)
} KlComponent;



// -----------------------------------------------------------------------------
//
// Component Vector
//
extern KlComponent klCompV[KL_COMPONENTS_MAX];
extern int         klCompNo;

#endif  // KLIB_KLCOMPONENT_H_
