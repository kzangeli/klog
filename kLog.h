// 
// FILE            kLog.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLOG_H_
#define KLOG_KLOG_H_

#include <stdio.h>                           // printf
#include <string.h>                          // strerror
#include <errno.h>                           // errno
#include <stdlib.h>                          // exit

#include "kbase/kBasicLog.h"                 // KBL_*

#include "klog/KlStatus.h"                   // KlsOk
#include "klog/klSem.h"                      // klSemTake + klSemGive
#include "klog/klTraceLevelV.h"              // klTraceLevelV
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klFreeText.h"                 // klFreeText
#include "klog/klMsgPrint.h"                 // klMsgPrint
#include "klog/klBufferPresent.h"            // klBufferPresent



#ifndef KL_ON
//
// all macros empty, except for actions like return/exit - no log
//
#define KL_V(compP, s)
#define KL_T(compP, tLev, s)
#define KL_M(compP, s)
#define KL_W(compP, s)
#define KL_E(compP, s)
#define KL_P(compP, s)
#define KL_RV(compP, s)     return
#define KL_RE(compP, rv, s) return rv
#define KL_RP(compP, rv, s) return rv
#define KL_X(compP, xc, s)  exit(xc)
#define KL_FIXME(compP, severity, s)
#define KL_READS(compP, buf, bytes)
#else
// -----------------------------------------------------------------------------
//
// KL_V - 
//
#define KL_V(compP, s)                                                                \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (compP->verbose == KTRUE)                                                      \
    {                                                                                 \
      if (klSemTake() == KlsOk)                                                       \
      {                                                                               \
        char* msg = klFreeText s;                                                     \
        klMsgPrint(compP, 'V', __FILE__, __LINE__, __FUNCTION__, msg, 0);             \
        klSemGive();                                                                  \
      }                                                                               \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_V(s);                                                                         \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_T - 
//
#define KL_T(compP, tLev, s)                                                          \
do                                                                                    \
{                                                                                     \
  int rLev = ((compP != NULL)? compP->offset : 0) + tLev;                             \
                                                                                      \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if ((rLev < KL_TRACE_LEVELS) && (klTraceLevelV[rLev] == KTRUE))                   \
    {                                                                                 \
      if (klSemTake() == KlsOk)                                                       \
      {                                                                               \
        char* msg = klFreeText s;                                                     \
        klMsgPrint(compP, 'T', __FILE__, __LINE__, __FUNCTION__, msg, tLev);          \
        klSemGive();                                                                  \
      }                                                                               \
    }                                                                                 \
  }                                                                                   \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_M - 
//
#define KL_M(compP, s)                                                                \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'M', __FILE__, __LINE__, __FUNCTION__, msg, 0);               \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_M(s);                                                                         \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_W - 
//
#define KL_W(compP, s)                                                                \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'W', __FILE__, __LINE__, __FUNCTION__, msg, 0);               \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_W(s);                                                                         \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_E - 
//
#define KL_E(compP, s)                                                                \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'E', __FILE__, __LINE__, __FUNCTION__, msg, 0);               \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_E(s);                                                                         \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_P - 
//
#define KL_P(compP, s)                                                                \
do                                                                                    \
{                                                                                     \
  int rNo = errno;                                                                    \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'P', __FILE__, __LINE__, __FUNCTION__, msg, rNo);             \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_E(s);                                                                         \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_RV - 
//
#define KL_RV(compP, s)                                                               \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'E', __FILE__, __LINE__, __FUNCTION__, msg, 0);               \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_E(s);                                                                         \
                                                                                      \
  return;                                                                             \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_RE - 
//
#define KL_RE(compP, rv, s)                                                           \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'E', __FILE__, __LINE__, __FUNCTION__, msg, 0);               \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_E(s);                                                                         \
                                                                                      \
  return rv;                                                                          \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_RP - 
//
#define KL_RP(compP, rv, s)                                                           \
do                                                                                    \
{                                                                                     \
  int rNo = errno;                                                                    \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'P', __FILE__, __LINE__, __FUNCTION__, msg, rNo);             \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_P(s);                                                                         \
                                                                                      \
  return rv;                                                                          \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_X - 
//
#define KL_X(compP, xc, s)                                                            \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if (klSemTake() == KlsOk)                                                         \
    {                                                                                 \
      char* msg = klFreeText s;                                                       \
      klMsgPrint(compP, 'E', __FILE__, __LINE__, __FUNCTION__, msg, 0);               \
      klSemGive();                                                                    \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
    KBL_E(s);                                                                         \
                                                                                      \
  exit(xc);                                                                           \
} while (0)



// -----------------------------------------------------------------------------
//
// KL_FIXME - 
//
#define KL_FIXME(compP, severity, s)                                                  \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if ((compP != NULL) && (compP->fixme == KTRUE))                                   \
    {                                                                                 \
      if (klSemTake() == KlsOk)                                                       \
      {                                                                               \
        char* msg = klFreeText s;                                                     \
        klMsgPrint(compP, 'F', __FILE__, __LINE__, __FUNCTION__, msg, severity);      \
        klSemGive();                                                                  \
      }                                                                               \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
  {                                                                                   \
    KBL_FIXME(severity, s);                                                           \
  }                                                                                   \
} while (0)




// -----------------------------------------------------------------------------
//
// KL_READS - 
//
#define KL_READS(compP, buf, bytes)                                                   \
do                                                                                    \
{                                                                                     \
  if (compP != NULL)                                                                  \
  {                                                                                   \
    if ((compP != NULL) && (compP->reads == KTRUE))                                   \
    {                                                                                 \
      if (klSemTake() == KlsOk)                                                       \
      {                                                                               \
        klBufferPresent(compP, 'R', __FILE__, __LINE__, __FUNCTION__, buf, bytes);    \
        klSemGive();                                                                  \
      }                                                                               \
    }                                                                                 \
  }                                                                                   \
  else                                                                                \
  {                                                                                   \
    KBL_READS(buf, bytes);                                                            \
  }                                                                                   \
} while (0)


#endif  // KL_ON

#endif  // KLOG_KLOG_H_
