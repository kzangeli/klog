// 
// FILE            klComponentPresent.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KLOG_KLCOMPONENTPRESENT_H_
#define KLOG_KLCOMPONENTPRESENT_H_

#include "klog/KlComponent.h"                // KlComponent
#include "klog/klComponentPresent.h"         // Own interface



// -----------------------------------------------------------------------------
//
// klComponentPresent - present (on screen) a component
//
// oneOfMany to be passed as KTRUE *only* when called by klComponentsPresent
//
extern void klComponentPresent(KlComponent* compP, KBool oneOfMany);

#endif  // KLOG_KLCOMPONENTPRESENT_H_
