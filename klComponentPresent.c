// 
// FILE            klComponentPresent.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kMacros.h"                   // K_SET
#include "kbase/kTypes.h"                    // KBool

#include "klog/KlComponent.h"                // KlComponent
#include "klog/klTraceLevelV.h"              // klTraceLevelV
#include "klog/klComponentPresent.h"         // Own interface



// -----------------------------------------------------------------------------
//
// klComponentPresent - present (on screen) a component
//
void klComponentPresent(KlComponent* compP, KBool oneOfMany)
{
#ifdef KBLOG_ON
  int    ix;
  char*  indent = "";

  if (oneOfMany == KTRUE) // Called from klComponentsPresent, presumably
  {
    KBL_M(("\n%s:", compP->name));
    indent = "  ";
  }
  else
  {
    KBL_M(("\n\n---------------------------------------------------"));
    KBL_M(("Component:       %s", compP->name));
  }

  KBL_M(("%sIn Use:          %s", indent, K_FT(compP->inUse)));
  KBL_M(("%sTrace Levels:    %d", indent, compP->tLevs));
  KBL_M(("%sTrace Offset:    %d", indent, compP->offset));
  KBL_M(("%sVerbose:         %s", indent, K_FT(compP->verbose)));
  KBL_M(("%sFixme:           %s", indent, K_FT(compP->fixme)));
  KBL_M(("%sDoubt:           %s", indent, K_FT(compP->doubt)));

  for (ix = 0; ix < compP->tLevs; ix++)
  {
    int   vectorIx = compP->offset + ix;
    KBool on       = klTraceLevelV[vectorIx];

    KBL_M(("%sTrace Level %03d:  %-5s - \"%s\"", indent, compP->tLevInfoV[ix].ix, K_SET(on), compP->tLevInfoV[ix].name));
  }

  KBL_M(("---------------------------------------------------\n\n"));
#endif
}
