// 
// FILE            klVerboseLevelSet.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "klog/KlStatus.h"                   // KlStatus
#include "klog/KlComponent.h"                // KlComponent
#include "klog/klComponentLookup.h"          // klComponentLookup, klGlobalComponentGet
#include "klog/klHooks.h"                    // KL_ERROR_HOOK, ...
#include "klog/klVerboseLevelSet.h"          // Own interface



// -----------------------------------------------------------------------------
//
// klVerboseLevelSet -
//
// Format of 'verboseLevels' is: "comp1,comp2,comp3,...".
//
// I.e. a comma separated list of component names.
// Two special values: "ALL" and "NONE"
// The input string can *only* contain the chars a-z, A-Z, underscore, hyphen, comma, and digits.
//
KlStatus klVerboseLevelSet(KlComponent* compP, char* verbose)
{
  char* start = verbose;
  KBool done  = KFALSE;
  KBool ready = KFALSE;

  if (strcmp(verbose, "All") == 0)
  {
    //
    // Set verbose level for ALL components, including 'KBL_V'
    //
    kbVerbose = KTRUE;

    int ix;
    for (ix = 0; ix < klCompNo; ix++)
    {
      if (klCompV[ix].inUse == KTRUE)
        klCompV[ix].verbose = KTRUE;
    }

    return KlsOk;
  }

  while (done == KFALSE)
  {
    if (*verbose == 0)
    {
      ready = KTRUE;
      done  = KTRUE;  // As soon as 'start' has been processed ...
    }
    else if (*verbose == ',')
    {
      *verbose = 0;
      ready = KTRUE;
    }
    else if ((*verbose >= 'a') && (*verbose <= 'z'));    // a-z: OK
    else if ((*verbose >= 'A') && (*verbose <= 'Z'));    // A-Z: OK
    else if ((*verbose >= '0') && (*verbose <= '9'));    // 0-9: OK
    else if (*verbose == '_');                           // underscore: OK
    else
    {
      KBL_E(("invalid char in verbose level: '%c' (0x%x)", *verbose, *verbose & 0xFF));
      KL_ERROR_HOOK(KlsInvalidCharacter, "invalid char in verbose level");
      return KlsInvalidCharacter;
    }

    if (ready == KTRUE)
    {
      if (start[0] == 0)
      {
        KBL_E(("Empty component name"));
        KL_ERROR_HOOK(KlsNoComponent, "Empty component name for verbose level");
        return KlsNoComponent;
      }
    
      if ((strcmp(start, "kbase") == 0) || (strcmp(start, "klog") == 0))
      {
        kbVerbose = KTRUE;
      }
      else
      {
        KlComponent* compP = klComponentLookup(start);

        if (compP == NULL)
        {
          KBL_E(("Component not found: '%s'", start));
          KL_ERROR_HOOK(KlsComponentNotFound, "Component for verbose level not found");
          return KlsComponentNotFound;
        }
    
        compP->verbose = KTRUE;
      }
      
      //
      // Prepare for the rest of the loop (unless done == KTRUE)
      //
      if (done == KTRUE)
        break;
      
      ready = KFALSE;
      start = &verbose[1];
    }
    
    ++verbose;
  }

  return KlsOk;
}
